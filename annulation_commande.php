<?php
if(isset($_POST['identifiant']))
{
// On se connecte � MySQL
require 'include/mysql.inc.php';
		setlocale(LC_TIME, 'french');
	date_default_timezone_set('Europe/Paris');
	$heure = date("H:i:s");
	$date_actuel = date("Y-m-d");
	$jour_actuel = strftime('%A', strtotime($date_actuel));
	$heure_ouverture = date("07:00:00");				//ouverture du site
	$heure_fermeture_panier = date("11:00:00");
	$heure_fermeture = date("11:00:00");
	if($jour_actuel != "samedi" && $jour_actuel != "dimanche")
	{
		if(($heure_ouverture < $heure && $heure_fermeture > $heure))
		{
			$heure_du_jour_ouverture = date("Y-m-d 7:00:00");
			$heure_du_jour_fermeture = date("Y-m-d 11:00:00");
			$req = $bdd->prepare('UPDATE commandes SET confirmation=0 WHERE id_client =? && date_cree > ? && date_cree < ?');
			$req->execute(array($_POST['identifiant'],$heure_du_jour_ouverture,$heure_du_jour_fermeture));
			$_SESSION['info_panier'] = "Votre commande a �t� annul�e";
			header('Location: panier.php');
			
		}
		else
		{
			$_SESSION['info_panier'] = "Op�ration impossible, la plateforme n'est active que de 7h � 11h.";
			header('Location: panier.php');
			exit;
		}
	}
	else
	{
		$_SESSION['info_panier'] = "Op�ration impossible, La plateforme n'est pas activ�e le weekend.";
		header('Location: panier.php');
		exit;
	}				
			
}
?>