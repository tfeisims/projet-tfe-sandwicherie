-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 15 Mars 2014 à 19:10
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bd_sandwicherie`
--
CREATE DATABASE IF NOT EXISTS `bd_sandwicherie` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_sandwicherie`;

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `pro_produits`()
select * from produits$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `commentaire` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`, `commentaire`) VALUES
(1, 'Sandwichs', ''),
(2, 'Repas chauds', ''),
(3, 'Repas froids', ''),
(4, 'Pains chauds', ''),
(5, 'Autres', ''),
(6, 'Boissons chaudes', ''),
(7, 'Viennoiseries', ''),
(8, 'Boissons froides', ''),
(9, 'Suppléments', ''),
(10, 'Sauce', '');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL,
  `compte` varchar(255) DEFAULT 'visiteur',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`id`, `nom`, `prenom`, `mot_de_passe`, `compte`) VALUES
('kevin.detrain', 'detrain', 'kevin', 'azerty', 'administrateur'),
('logan.detrain', 'detrain', 'logan', 'wanted007', 'visiteur'),
('marc.fanuel', 'fanuel', 'marc', 'azerty', 'visiteur'),
('test', 'test', 'test', 'test', 'visiteur');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` varchar(255) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `quantite` tinyint(4) DEFAULT '0',
  `date_cree` datetime NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `confirmation` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_produit` (`id_produit`),
  KEY `id_client` (`id_client`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Contenu de la table `commandes`
--

INSERT INTO `commandes` (`id`, `id_client`, `id_produit`, `quantite`, `date_cree`, `commentaire`, `confirmation`) VALUES
(16, 'kevin.detrain', 3, 3, '2014-02-17 11:17:43', '', 1),
(17, 'kevin.detrain', 27, 2, '2014-02-17 11:17:43', '', 1),
(18, 'kevin.detrain', 2, 5, '2014-02-17 18:08:55', 'pas trop d''andalouse', 1),
(19, 'kevin.detrain', 1, 5, '2014-02-17 18:08:55', 'pas trop d''andalouse', 1),
(20, 'kevin.detrain', 2, 1, '2014-02-18 12:21:43', '', 1),
(21, 'kevin.detrain', 4, 5, '2014-02-19 20:07:33', 'pas trop de poivre', 1),
(22, 'kevin.detrain', 3, 3, '2014-02-20 19:12:02', '', 0),
(23, 'kevin.detrain', 33, 1, '2014-02-20 19:12:02', '', 0),
(24, 'kevin.detrain', 30, 2, '2014-02-20 19:20:19', 'pas trop de jambon ;)', 0),
(25, 'kevin.detrain', 2, 2, '2014-02-20 20:36:13', '', 0),
(26, 'kevin.detrain', 30, 5, '2014-02-20 20:36:13', '', 0),
(27, 'kevin.detrain', 6, 2, '2014-02-20 20:36:13', '', 0),
(28, 'kevin.detrain', 2, 4, '2014-02-21 10:01:27', '', 1),
(29, 'kevin.detrain', 33, 1, '2014-02-21 10:01:27', '', 1),
(30, 'kevin.detrain', 3, 4, '2014-02-24 09:03:06', '', 0),
(31, 'kevin.detrain', 30, 4, '2014-02-24 09:06:55', '', 1),
(32, 'kevin.detrain', 1, 5, '2014-02-25 08:50:21', '', 1),
(33, 'kevin.detrain', 1, 5, '2014-02-26 18:45:13', 'pas trop de poulet sur le panini poulet :p', 1),
(34, 'kevin.detrain', 29, 5, '2014-02-26 18:45:13', 'pas trop de poulet sur le panini poulet :p', 1),
(35, 'kevin.detrain', 3, 4, '2014-03-09 16:45:19', 'pas trop de jambon ;)', 1),
(36, 'kevin.detrain', 8, 1, '2014-03-09 16:45:19', 'pas trop de jambon ;)', 1),
(37, 'kevin.detrain', 3, 1, '2014-03-10 19:22:16', 'beaucoup de fromage', 1),
(38, 'kevin.detrain', 7, 4, '2014-03-10 19:22:16', 'beaucoup de fromage', 1),
(39, 'logan.detrain', 3, 4, '2014-03-10 21:00:25', 'merci beaucoup :)', 1),
(40, 'logan.detrain', 39, 2, '2014-03-10 21:00:25', 'merci beaucoup :)', 1),
(41, 'test', 24, 2, '2014-03-10 21:02:49', 'merci :)', 1),
(42, 'test', 22, 1, '2014-03-10 21:02:49', 'merci :)', 1),
(43, 'test', 49, 2, '2014-03-10 21:02:49', 'merci :)', 1),
(44, 'kevin.detrain', 3, 4, '2014-03-12 15:41:37', 'pas trop de salade', 1),
(45, 'kevin.detrain', 21, 2, '2014-03-12 15:41:37', 'pas trop de salade', 1),
(46, 'kevin.detrain', 3, 4, '2014-03-13 15:20:24', 'pas trop de salade', 1),
(49, 'test', 13, 2, '2014-03-13 15:22:25', 'blablabla1', 1),
(50, 'test', 51, 2, '2014-03-13 15:22:25', 'blablabla1', 1),
(51, 'logan.detrain', 3, 3, '2014-03-13 15:25:57', 'blablabla2', 1),
(52, 'logan.detrain', 20, 1, '2014-03-13 15:25:57', 'blablabla2', 1),
(53, 'logan.detrain', 11, 2, '2014-03-13 15:27:35', 'blablabla3', 1);

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `id_categorie_image` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=248 ;

--
-- Contenu de la table `images`
--

INSERT INTO `images` (`id`, `titre`, `url`, `id_categorie_image`) VALUES
(209, NULL, 'img/uploads/f1.jpg', 1),
(210, NULL, 'img/uploads/f2.PNG', 1),
(211, NULL, 'img/uploads/f3.png', 1),
(212, NULL, 'img/uploads/f4.jpg', 1),
(213, NULL, 'img/uploads/f4.jpg', 1),
(214, NULL, 'img/uploads/f4.jpg', 1),
(215, NULL, 'img/uploads/f7.jpg', 1),
(216, NULL, 'img/uploads/f8.jpg', 1),
(217, NULL, 'img/uploads/f9.jpg', 1),
(218, NULL, 'img/uploads/f9.jpg', 1),
(219, NULL, 'img/uploads/f9.jpg', 1),
(220, NULL, 'img/uploads/f12.jpg', 1),
(221, NULL, 'img/uploads/f13.jpg', 1),
(222, NULL, 'img/uploads/f14.jpg', 1),
(223, NULL, 'img/uploads/f14.jpg', 1),
(224, NULL, 'img/uploads/f16.jpg', 1),
(225, NULL, 'img/uploads/f16.jpg', 1),
(226, NULL, 'img/uploads/f18.jpg', 1),
(227, NULL, 'img/uploads/f18.jpg', 1),
(228, NULL, 'img/uploads/f20.jpg', 1),
(229, NULL, 'img/uploads/f20.jpg', 1),
(230, NULL, 'img/uploads/f22.jpg', 1),
(231, NULL, 'img/uploads/f23.jpg', 1),
(232, NULL, 'img/uploads/f23.jpg', 1),
(233, NULL, 'img/uploads/f25.jpg', 1),
(234, NULL, 'img/uploads/f26.jpg', 1),
(235, NULL, 'img/uploads/f27.jpg', 1),
(236, NULL, 'img/uploads/f27.jpg', 1),
(237, NULL, 'img/uploads/f29.jpg', 1),
(238, NULL, 'img/uploads/f29.jpg', 1),
(239, NULL, 'img/uploads/f31.jpg', 1),
(240, NULL, 'img/uploads/f31.jpg', 1),
(241, NULL, 'img/uploads/f33.jpg', 1),
(242, NULL, 'img/uploads/f33.jpg', 1),
(243, NULL, 'img/uploads/f35.jpg', 1),
(244, NULL, 'img/uploads/f36.jpg', 1),
(245, NULL, 'img/uploads/f37.jpg', 1),
(246, NULL, 'img/uploads/f37.jpg', 1),
(247, NULL, 'img/uploads/f39.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_cree` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prix` float NOT NULL,
  `id_categorie` int(100) NOT NULL,
  `quantite_restante` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `disponible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ic_categorie` (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `prix`, `id_categorie`, `quantite_restante`, `description`, `disponible`) VALUES
(1, 'Pordalou', 2, 1, 10, 'Rôti de port,sauce andalouse,tomates', 0),
(2, 'Végétarien', 2, 1, 10, 'Crudités du jour,mayonaise', 0),
(3, 'Niçois', 2, 1, 10, 'Thon mayonnaise, anchois, olives, salade', 1),
(4, 'Poivre', 2, 1, 10, 'Poulet grillé, sauce au poivre, salade', 0),
(5, 'Italo-Belge', 2, 1, 10, 'Jambon d''Ardenne, mozzarella, huile d''olive, tomates', 1),
(6, 'Boulette, andalouse, salade', 1.85, 1, 1, '', 1),
(7, 'Dagobert', 1.85, 1, 1, '', 1),
(8, 'Jambon', 1.85, 1, 1, '', 1),
(9, 'Fromage', 1.85, 1, 1, '', 1),
(10, 'Thon mayonnaise', 1.85, 1, 1, '', 1),
(11, 'Thon piquant', 1.85, 1, 1, '', 1),
(12, 'Crabe surimi', 1.85, 1, 1, '', 1),
(13, 'Poulet andalouse', 1.85, 1, 1, '', 1),
(14, 'Poulet curry', 1.85, 1, 1, '', 1),
(15, 'Americain', 1.85, 1, 1, '', 1),
(16, 'Filet de poulet', 1.85, 1, 1, '', 1),
(17, 'Pain de viande maison', 1.85, 1, 1, 'à partir du mardi', 1),
(18, 'Pitta', 1.85, 1, 1, '', 1),
(19, 'Lasagne bolognaise', 4, 2, 1, '', 1),
(20, 'Boulettes sauce tomate + frites', 4, 2, 1, '', 1),
(21, 'Pennes au 4 fromages', 4, 2, 1, '', 1),
(22, 'Spaghetti carbonara', 4, 2, 1, '', 1),
(23, 'Vol au vent + frites', 4, 2, 1, '', 1),
(24, 'Américain + frites', 4, 3, 1, '', 1),
(25, 'Assiette de charcuteries + frites', 4, 3, 1, '', 1),
(26, 'Assiette filet de poulet + frites', 4, 3, 1, '', 1),
(27, 'Assiette jambon + frites', 4, 3, 1, '', 1),
(28, 'Pêches au thon (2) + frites', 4, 3, 1, '', 1),
(29, 'Panini poulet', 2, 4, 1, '', 1),
(30, 'Panini parme', 2, 4, 1, '', 1),
(31, 'Panini mozzarella', 2, 4, 1, '', 1),
(32, 'Panini jambon et fromage', 2, 4, 1, '', 1),
(33, 'Pain hamburger', 2, 5, 1, '', 1),
(34, 'Portion de frites + sauce', 2, 5, 1, '', 1),
(35, 'Mitraillette hamburger', 3.5, 5, 1, '', 1),
(36, 'Café', 0.7, 6, 1, '', 1),
(37, 'Thé', 0.7, 6, 1, '', 1),
(38, 'Chocolat chaud', 0.7, 6, 1, '', 1),
(39, 'Pain au chocolat', 0.7, 7, 1, '', 1),
(40, 'Croissant', 0.7, 7, 1, '', 1),
(41, 'Coca', 0.7, 8, 1, '', 1),
(42, 'Coca light', 0.7, 8, 1, '', 1),
(43, 'Fanta citron', 0.7, 8, 1, '', 1),
(44, 'Fanta orange', 0.7, 8, 1, '', 1),
(45, 'Ice tea pétillant', 0.7, 8, 1, '', 1),
(46, 'Jus d''orange', 0.7, 8, 1, '', 1),
(47, 'Eau pétillantes', 0.7, 8, 1, 'Spa', 1),
(48, 'Eau plate', 0.7, 8, 1, 'Valvert', 1),
(49, 'Cécémel', 0.7, 8, 1, '', 1),
(50, 'Salade', 0, 9, 1, '', 1),
(51, 'Tomate', 0, 9, 1, '', 1),
(55, 'Pain', 0.2, 9, 1, '', 1),
(56, 'Anchois', 0, 9, 1, '', 1),
(57, 'Mayonnaise', 0, 10, 999, '', 1),
(58, 'Ketchup', 0, 10, 999, '', 1),
(59, 'Andalouse', 0, 10, 999, '', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `id_client` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `id_produit` FOREIGN KEY (`id_produit`) REFERENCES `produits` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `ic_categorie` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
