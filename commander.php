<!-----
réalialiser par: kevin detrrain,
but: projet TFE
année: 2014
utilité:
cette page affiche les produits proposés par la sandwicherie
------>
<?php
session_start();
require_once 'class/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="language" content="FR" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="style.css" />
		<link rel="icon" type="image/png" href="img/favicon.ico" />
		<script type="text/javascript" src="jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="style_ie.css" />
        <![endif]-->
	<?php
		$check = $detect->isTablet();
		$check2 = $detect->isMobile();
		//s'il s'agit d'un mobile on applique le style mobile
		if($check2 || $check)
		echo '<link rel="stylesheet" href="style_mobile.css" />';
	?>
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
		?>
		<div id="conteneur_principal">
			<!-----zone central contenant les éléments important---------------------->
			<div id="zone_affichage">
				<?php
					setlocale(LC_TIME, 'french');
					date_default_timezone_set('Europe/Paris');
					$heure = date("H:i:s");
					$date_actuel = date("Y-m-d");
					$jour_actuel = strftime('%A', strtotime($date_actuel));
					$heure_ouverture = date("07:00:00");				//ouverture du site
					$heure_fermeture_panier = date("11:00:00");
					$heure_fermeture = date("13:30:00");
				//------------------------------------------------------------------------------alerte
					if(isset($_SESSION['erreur_panier'])) //avertissement
					{
						echo 	"<div class='cadre_alerte'>
									<img src='img/attention.png' alt='img attention' id='img_attention'/>  <span id='texte_alerte'>"
									. $_SESSION['erreur_panier'] ."</span></div>";
									$_SESSION['erreur_panier'] = NULL;
					}
					if(isset($_SESSION['info_panier'])) //information
					{
						echo 	"<div class='cadre_alerte'>
									<img src='img/info.png' alt='img info' id='img_info'/>  <span id='texte_alerte'>"
									. $_SESSION['info_panier'] ."</span></div>";
									$_SESSION['info_panier'] = NULL;
					}
					if((empty($_SESSION['info_panier']) && empty($_SESSION['erreur_panier'])) && ($jour_actuel == "samedi" || $jour_actuel == "dimanche")) // information si hors période (jours)
					{
					echo 	"<div class='cadre_alerte'>
									<img src='img/info.png' alt='img info' id='img_info'/>  <span id='texte_alerte'>Nous sommes ".$jour_actuel.", la plateforme n'est pas activée aujourd'hui.</span>
								</div>";
					}
					if((empty($_SESSION['info_panier']) && empty($_SESSION['erreur_panier'])) && ($heure_ouverture > $heure || $heure_fermeture_panier < $heure) && ($jour_actuel != "samedi" && $jour_actuel != "dimanche")) // information si hors période (heure)
					{
					echo 	"<div class='cadre_alerte'>
									<img src='img/info.png' alt='img info' id='img_info'/>  <span id='texte_alerte'>Il est ".$heure.", la plateforme n'est active que de 7h à 11h.</span>
								</div>";
					}
				//---------------------------------------------------------------------------------------------	
				?>
				<h1>Commander</h1>
				<?php
					// On se connecte à  MySQL
					include_once 'include/mysql.inc.php';
					try
					{
					$req = $bdd->query('SELECT c.id,c.nom as nom_cat,p.id as id_produit, p.nom as nom_produit, p.prix as prix_produit,p.id_categorie,p.disponible,p.supprimer
					FROM categorie as c
					INNER JOIN produits as p
					ON c.id= p.id_categorie
					WHERE p.supprimer=0
					ORDER BY c.id,p.prix DESC,p.nom DESC
					') or die(print_r($bdd->errorInfo()));
					//ligne 1: on selection les élément comme ceci c (alias de categorie)  .nom(champs) as nom_cat (on donne un alias a l'élément) , idem
					//ligne 2 et 3: on précise les tables et on précise la type de jointure (inner join = jointure interne (on prend tout les éléments sauf ceux qui sont égale à  null), right join = jointure externe(on //prend tout les éléments même ceux qui sont égale à null)
					//ligne 4: on précise les élément qui sont lié (ps: il doivent etre du meme type et meme valeur)
					//ligne 5: on peut trier les résultat (where,ordre by, limit 0,5)
					//  /!\ il est préférable d'utilisé "or die(print_r($bdd->errorInfo())) " pour obtenir une erreur écrite plus correctement.
					$categorie_ecrit = null;
					$compteur = 0;

					while ($donnees = $req->fetch())
					 {	//---------------	permet d'obtenir un titre qui est égale à chaque catégorie et de regrouper les produit de cette catégorie
						 if ($categorie_ecrit != $donnees["nom_cat"])
						 {
							if($categorie_ecrit != null)				//-- on ferme le tableau au 2eme tour pour le tableau contenant le premier type de catégorie
							{	echo "</table>";						//--------------fin du tableau de produit prodduit
								echo "</div>";							//--------------fin du cadre compteneur
								echo "</div>";																			
							}
							
							$compteur +=1;
							/*
							cellule: conteneur principal contenant le div (titre_cellule) et le div (compteneur_produit)
							
							div cellule
							{
									-titre_cellule
									{
										<b>catégorie</b>
										<im V>
									}
									-compteneur_produit
									{
										tableau
									}
							}
							*/
											 
							$categorie_ecrit = $donnees["nom_cat"];
							echo "<div class='cellule'>";
							//---------------début du cadre titre
							echo "<div id='t" . $compteur ."'  class='titre_cellule'>";	
								echo "<b>" . $categorie_ecrit ."</b><img src='img/triangle_inverse.png' alt='image triangle inversé' class='triangle_inverse'/>";
							//---------------fin du cadre titre
							echo "</div>";																																						
							//-------------début du cadre compteneur
							echo "<div id=c" . $compteur . " class='compteneur_produit'>";	
							//--------------début du tableau de produit
							echo "<table id='p" . $compteur . "' cellspacing=0; cellpadding=2;>";			 							
							$compteur_bg = 0;
						 }
						//-------------variation de couleurs dans les différentes colonnes
					 echo "<tr ";
						if($compteur_bg%2 == 1)
							echo "class='font_clair'>";
						else
							echo "class='font_foncer'>";
							$compteur_bg+=1;
							echo"<td class='ligne_nom_produit'>" . $donnees['nom_produit'] . "</td>
									<td class='ligne_prix_produit'>" . $donnees['prix_produit'] . " €</td>
									<td class='ligne_detail'><a href='#' class='lien_detail'>détail</a></td>
									<td class='ligne_entree_nombre'><form method='post' action='verification/addPanier.php'><input type='hidden' name='id_produit' value='".$donnees['id_produit']."'>";
									if($donnees['prix_produit'] != 0)	//si le produits cout 0 € on  demande au client si il en veut ou pas
									{
										if($donnees['disponible'] == 1)
										{
											echo "<span ><input type='number' name='quantite' class='taille_champs_nombre'></span>";
										}
										else
										{
											echo "<span ><input type='number' disabled='disabled' name='quantite' class='taille_champs_nombre'></span>";
										}
									}
									else
									{
										if($donnees['disponible'] == 1)
										{
											echo "<span><select  name='quantite' class='champs_disponible'><option value='0' selected='selected'>Non</option><option value='1'>Oui</option></select></span>";
										}
										else
										{
											echo "<span><select  name='quantite' disabled='disabled' class='champs_disponible'><option value='0' selected='selected'>Non</option><option value='1'>Oui</option></select></span>";
										}
									}
							echo"<span  class='ligne_valider'>";
									if($donnees['disponible'] == 1)
									{
										echo "<input type='submit' class='bouton_commander' value='Commander'>";
									}
									else
									{
										echo "<input type='submit' class='bouton_commander' value='Indisponible' disabled='disabled'>";
									}
								echo  "</form></span></td></tr>";									
					 
					 }
						echo "</table>";																		//--------------fin du dernier cadre produit
						echo "</div>";																		//--------------fin du dernier cadre compteneur
						echo "</div>";
					 $req->closeCursor();
					}
		 catch (PDOException $e){
			 //echo 'Erreur de connexion !!! :' . $e->getMessage() . '<br/>';
			echo 'une erreur s\'est produite sur la page, veuillez contacter le service informatique.<br/>';
			 exit();
		 }		 
	 		?>						
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script>
		// affiche et cache les produits
		<?php	
		$req = $bdd->query('SELECT  COUNT(*) as total FROM categorie ') or die(print_r($bdd->errorInfo()));
		$total_categorie= $req->fetch();
		$req->closeCursor();
			
			//je génére le code permettant de de faire apparaitre et disparaitre les différents produits
			for($var=1 ; $var<=$total_categorie['total']; $var++)
					{
						echo	"$('#t". $var . " ').click(function(){
									$('#c". $var ." ').stop().slideToggle(800);"; 
					//cette partie permet de cacher les éléments déja afficher				
					for($var2=1 ; $var2<=$total_categorie['total']; $var2++)
							{
								if($var2 != $var)
								{
									echo "$('#c". $var2 ." ').stop().slideUp(800);";
								}
							}
						
						echo 		"});";
					}
		?>
		</script>
		<script type="text/javascript" src="js/monJQ.js"></script>	
	</body>
</html>