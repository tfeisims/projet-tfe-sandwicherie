<?php
session_start();
include ('verification/verification_acces.php');

// On se connecte � MySQL
require 'include/mysql.inc.php';

//variables pour r�cup�rer les commandes r�alis�
$heure_du_jour_ouverture = date("Y-m-d 7:00:00");		// r�cup�ration dans la base( p�riode o� l'on prend compte de la commande r�alis� dans la journ�e)
$heure_du_jour_fermeture = date("Y-m-d 23:00:00");		// idem

if(isset($_POST['id_produit']) && isset($_POST['id_client']) && isset($_POST['quantite'])) // modifier
{
	if($_POST['quantite'] > 0)
	{
	$req = $bdd->prepare('UPDATE commandes SET quantite=? WHERE id_produit=? && id_client =? && date_cree > ? && date_cree < ?');
	$req->execute(array($_POST['quantite'],$_POST['id_produit'],$_POST['id_client'],$heure_du_jour_ouverture,$heure_du_jour_fermeture));
	header('Location: commande_admin.php');	
	exit;
	}
	else
	{
		header('Location: commande_admin.php');
		exit;
	}
}
else if( isset($_GET['id_client']) && isset($_GET['id_produit']))  // supprimer
{
	$req = $bdd->prepare('UPDATE commandes SET confirmation=0 WHERE id_produit=? && id_client =? && date_cree > ? && date_cree < ?');
	$req->execute(array($_GET['id_produit'],$_GET['id_client'],$heure_du_jour_ouverture,$heure_du_jour_fermeture));
	header('Location: commande_admin.php');		
	exit;
}
else if(isset($_POST['nom_produit']) && isset($_POST['quantite']) && isset($_POST['id_client']))
{
	if($_POST['quantite'] > 0)
	{
		$resultat = explode(".", $_POST['nom_produit']); // on r�cup�re uniquement l'id dans le nom du proudit
		$req = $bdd->prepare('INSERT INTO commandes (id_client, id_produit,quantite, date_cree,commentaire,confirmation) VALUES(:id_client,:id_produit, :quantite, NOW(), :commentaire, :confirmation)');
		$req->execute(array(
					'id_client' => $_POST['id_client'],
					'id_produit' => $resultat[0],
					'quantite' => $_POST['quantite'],
					'commentaire' => '',
					'confirmation' => "1"					
					)) or die(print_r($bdd->errorInfo()));	
		header('Location: commande_admin.php');					
	}
	else
	{
		header('Location: commande_admin.php');		
		exit;
	}
}
else // si ne correspond � rien
{
		header('Location: commande_admin.php');	
		exit;
}

?>