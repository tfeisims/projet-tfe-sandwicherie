<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />		
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="../style_ie.css" />
        <![endif]-->
	<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
		?>	
		<div id="conteneur_principal">
			<!-----zone central contenant les élément important---------------------->
			<div id="zone_affichage">
					<h1>Informations</h1>
					<p>La cafétéria est actuellement ouverte du lundi au vendredi,</p>
					<p>de 08h10 à 08h30<br/>de 10h00 à 10h30<br/>de 12h30 à 13h30</p>
					
					<h2>Acquisition de la commande</h2>
					<p style='text-align:justify;'>Lors de la récupération de votre commande, vous devez présenter votre carte étudiant que vous avez obtenu lors de votre inscription.</p>
					<p style='text-align:justify;'>Assurez-vous d'avoir des fonts suffisants sur votre carte étudiant, tout achats avec des fonds insuffissants vous sera facturé par courrier.</p>
					
					<h2>Annulation de commande</h2>
					<p style='text-align:justify;'>Vous avez la possibilité d'annuler votre commande à tout moment avant 11h. Après ce délai, votre commande sera validée et il ne vous sera plus possible de l'annuler.</p>
					
					<h2>Remarques</h2>
					<p style='text-align:justify;'>L'accés à la cafétéria est strictement réservé aux étudiants, aux professeurs et au personnel fréquentant l'établissement.</p>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script type="text/javascript" src="js/monJQ.js"></script>	
	</body>
</html>