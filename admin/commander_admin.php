﻿<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />	
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if gt IE 7]>
			<link rel="stylesheet" href="../style_ie.css" />
        <![endif]-->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
		?>
		<div id="conteneur_principal">
			<!-----zone central contenant les éléments important---------------------->
			<div id="zone_affichage">
				<?php
					if(isset($_SESSION['erreur_commander_admin']))
					{
						echo 	"<div class='cadre_alerte'>
									<img src='../img/attention.png' alt='img attention' id='img_attention'/>  <span id='texte_alerte'>"
									. $_SESSION['erreur_commander_admin'] ."</span></div>";
									$_SESSION['erreur_commander_admin'] = NULL;
					}
					if(isset($_SESSION['info_commander_admin']))
					{
						echo 	"<div class='cadre_alerte'>
									<img src='../img/info.png' alt='img info' id='img_info'/>  <span id='texte_alerte'>"
									. $_SESSION['info_commander_admin'] ."</span></div>";
									$_SESSION['info_commander_admin'] = NULL;
					}
				?>
				<h1>Gestion de la liste des produits</h1>
				<?php
					// On se connecte à  MySQL
					require 'include/mysql.inc.php';
					
					//on recupère le nombre de catégorie total pour évité que l'on introduise un numéro de catégorie qui n'existe pas
					$req = $bdd->query('SELECT COUNT(id) AS total_cat FROM categorie');
					$total_cat = $req->fetch();
					$req->closeCursor();
					$date_semaine_passee = strftime("%Y-%m-%d", mktime(0, 0, 0, date('m'), date('d')-7, date('y'))); 
					
					$req = $bdd->query('SELECT c.id as id_cat,c.nom as nom_cat,p.id as id_produit, p.nom as nom_produit, p.prix as prix_produit,p.id_categorie, p.disponible, p.date_ajout,p.supprimer
					FROM categorie as c
					INNER JOIN produits as p
					ON c.id= p.id_categorie
					WHERE supprimer=0
					ORDER BY c.id
					') or die(print_r($bdd->errorInfo()));

					//ligne 1: on selection les élément comme ceci c (alias de categorie)  .nom(champs) as nom_cat (on donne un alias a l'élément) , idem
					//ligne 2 et 3: on précise les tables et on précise la type de jointure (inner join = jointure interne (on prend tout les éléments sauf ceux qui sont égale à  null), right join = jointure externe(on //prend tout les éléments même ceux qui sont égale à null)
					//ligne 4: on précise les élément qui sont lié (ps: il doivent etre du meme type et meme valeur)
					//ligne 5: on peut trier les résultat (where,ordre by, limit 0,5)
					//  /!\ il est préférable d'utilisé "or die(print_r($bdd->errorInfo())) " pour obtenir une erreur écrite plus correctement.
					$categorie_ecrit = null;
					$id_cat_ecrit = null;
					$compteur = 0;

					while ($donnees = $req->fetch())
					 {	//---------------	permet d'obtenir un titre qui est égale à chaque catégorie et de regrouper les produit de cette catégorie
						 if ($categorie_ecrit != $donnees["nom_cat"])
						 {
							if($categorie_ecrit != null)				//-- on ferme le tableau au 2eme tour pour le tableau contenant le premier type de catégorie
							{	//ligne de fin des cellules
								//création de la ligne d'ajout de produit
								echo"<form method='post' action='ajouter_commander.php' class='ligne_ajouter_produit'>
									<div class='ligne_nom_produit_admin'><input type='text' class='champs_nom_produit' name='nom_produit' value=''></div>
									<div class='ligne_prix_produit_admin'><input type='text' class='champs_prix_produit' name='prix_produit' value=''> €</div>
									<div class='ligne_detail_admin'><a href='#' class='lien_detail'>détails</a></div>
									<div class='ligne_disponible_admin'><select  name='disponible' class='champs_disponible'><option value='0'>Non</option><option value='1' selected='selected' >Oui</option></select></div>
									<div class='ligne_categorie_admin'><input type='number' disabled='disabled' name='categorie' value='".$id_cat_ecrit."' class='champs_categorie_ajout_admin'></div>
									<div  class='ligne_action_admin'><input type='hidden' name='categorie' value='".$id_cat_ecrit."'>
									<input type='submit' value='Ajouter' />
									</div>
									</form>";
								// fermeture du conteneur produit et de la cellule
								echo "</div>";							
								echo "</div>";																			
							}
							
							$compteur +=1;
							/*
							cellule: conteneur principal contenant le div (titre_cellule) et le div (compteneur_produit)
							
							div cellule
							{
									-titre_cellule
									{
										<b>catégorie</b>
										<im V>
									}
									-compteneur_produit
									{
										tableau
									}
							}
							*/
											 
							$categorie_ecrit = $donnees["nom_cat"];
							$id_cat_ecrit = $donnees['id_cat'];
							echo "<div class='cellule'>";
							//---------------début du cadre titre
							echo "<div id='t" . $compteur ."'  class='titre_cellule'>";	
								echo "<b> (".$id_cat_ecrit.") " . $categorie_ecrit ."</b><img src='../img/triangle_inverse.png' alt='image triangle inversé' class='triangle_inverse'/>";
							//---------------fin du cadre titre
							echo "</div>";																																						
							//-------------début du cadre compteneur
							echo "<div id=c" . $compteur . " class='compteneur_produit'>";		 							
							$compteur_bg = 0;
							//-------------titres des différentes colonnes
							echo"<div class='titre_des_produits'>
										<div class='titre_nom_produit_admin'>Produits</div>
										<div class='titre_prix_produit_admin'>Prix</div>
										<div class='titre_detail_admin'>Détails</div>
										<div class='titre_disponible_admin'>Disponibles</div>
										<div class='titre_categories_admin'>Catégories</div>
										<div class='titre_entree_action_admin'>Actions</div>
									</div>";
						 }
						//-------------variation de couleurs dans les différentes colonnes
					 echo "<form method='post' action='edit_commander.php' "; 	//tr
						if($compteur_bg%2 == 1)
							echo "class='font_clair'>";
						else
							echo "class='font_foncer'>";
							$compteur_bg+=1;
							//création des différentes lignes 
							echo"<div class='ligne_nom_produit_admin'><input type='text' class='champs_nom_produit' name='nom_produit' value='" . $donnees['nom_produit'] . "'></div>
									<div class='ligne_prix_produit_admin'><input type='text' class='champs_prix_produit' name='prix_produit' value='" . $donnees['prix_produit'] . "'> €</div>
									<div class='ligne_detail_admin'><a href='detail.php?id_produit=".$donnees['id_produit']."' class='lien_detail'>détails</a></div>
									<div class='ligne_disponible_admin'><select  name='disponible' class='champs_disponible'><option value='0'>Non</option><option value='1'"; if($donnees['disponible'] ==1){ echo "selected='selected'";}
							echo">Oui</option></select></div><div class='ligne_categorie_admin'><select name='categorie' class='champs_categorie_admin'>";
							//echo"<input type='number' name='categorie' value='".$donnees['id_cat']."' class='champs_categorie_admin'>";
							for($i=1;$i<=$total_cat['total_cat'];$i++)
							{
							echo "<option value='".$i."'"; if($i == $donnees['id_cat']){echo " selected='selected'";	}
							echo">".$i."</option>";
							}
							echo "</select></div><div  class='ligne_action_admin'><input type='hidden' name='id_produit' value='".$donnees['id_produit']."'><input type=image Value=submit class='b_editer_admin' title='Modifier' src='../img/editer.png' onmouseOver='mouse_over_img(this);' onmouseOut='mouse_out_img(this);'/><a href='suppression_commander.php?id_produit=".$donnees['id_produit']."' title='Supprimer'><img src='../img/corbeille.png' class='b_supprimer_admin' BORDER='0' alt='img edit' onmouseOver='mouse_over_img(this);' onmouseOut='mouse_out_img(this);'/></a>";
							if( $donnees['date_ajout'] >= $date_semaine_passee)	//limite mit sur le bouton permettant la publication d'une annonce
							{
								echo	"<a href='publier_commander.php?id_produit=".$donnees['id_produit']."' title='Publier une annonce'><img src='../img/publier.png' class='b_publier_admin' BORDER='0' alt='img edit' onmouseOver='mouse_over_img(this);' onmouseOut='mouse_out_img(this);'/></a>";
							}
							echo "</div>
									</form>";	//tr									
					 
					 }
					//ligne de fin de la dernier cellule
					//création de la ligne d'ajout de produit
						echo"<form method='post' action='ajouter_commander.php' class='ligne_ajouter_produit'>
						<div class='ligne_nom_produit_admin'><input type='text' class='champs_nom_produit' name='nom_produit' value=''></div>
						<div class='ligne_prix_produit_admin'><input type='text' class='champs_prix_produit' name='prix_produit' value=''> €</div>
						<div class='ligne_detail_admin'><a href='detail.php?id_produit=".$donnees['id_produit']."' class='lien_detail'>détails</a></div>
						<div class='ligne_disponible_admin'><select  name='disponible' class='champs_disponible'><option value='0'>Non</option><option value='1' selected='selected' >oui</option></select></div>
						<div class='ligne_categorie_admin'><input type='number' disabled='disabled' name='categorie' value='".$id_cat_ecrit."' class='champs_categorie_ajout_admin'></div>
						<div  class='ligne_action_admin'><input type='hidden' name='categorie' value='".$id_cat_ecrit."'>
						<input type='submit' value='Ajouter' />
						</div>
						</form>";
						echo "</div>";																		//--------------fin du dernier cadre produit
						echo "</div>";																		//--------------fin du dernier cadre compteneur
					 $req->closeCursor();
	 		?>						
					<div style="margin: 10px 1px 1px 5px; padding: 5px 5px 5px 5px;">Ajouter une nouvelle catégorie : <form method="post" action="#" style="display: inline-block;"><input type="text" name="nouvelle_cat" style="margin-left:2px;"/><input type="submit" value="Ajouter" style="margin-left:2px;"/></form></div>

			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script>
		// affiche et cache les produits
		<?php	
		$req = $bdd->query('SELECT  COUNT(*) as total FROM categorie ') or die(print_r($bdd->errorInfo()));
		$total_categorie= $req->fetch();
		$req->closeCursor();
			
			//je génére le code permettant de de faire apparaitre et disparaitre les différents produits
			for($var=1 ; $var<=$total_categorie['total']; $var++)
					{
						echo	"$('#t". $var . " ').click(function(){
									$('#c". $var ." ').stop().slideToggle(800);"; 
					//cette partie permet de cacher les éléments déja afficher				
					for($var2=1 ; $var2<=$total_categorie['total']; $var2++)
							{
								if($var2 != $var)
								{
									echo "$('#c". $var2 ." ').stop().slideUp(800);";
								}
							}
						
						echo 		"});";
					}
		?>
		</script>
		<script type="text/javascript" src="js/monJQ.js"></script>	
	</body>
</html>