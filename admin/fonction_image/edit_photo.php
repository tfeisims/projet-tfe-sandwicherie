<?php
session_start();
include ('../verification/verification_acces.php');
if(isset($_POST['commentaire']) && isset($_POST['lien']) && isset($_POST['identifiant']) && !empty($_POST['identifiant']))
{
	require '../include/mysql.inc.php';

$commentaire= htmlspecialchars($_POST['commentaire']);
$lien = htmlspecialchars($_POST['lien']);
$id = htmlspecialchars($_POST['identifiant']);	
	if(strlen($commentaire) <= 38)
	{
		$req=$bdd->prepare('UPDATE images SET titre = ? , lien_image = ? WHERE id= ?');
		$req->execute(array($commentaire,$lien,$id));
		$_SESSION['info_gestion_image_admin'] = "L'image a bien été modifier.";
		header('location: ../gerer_foto.php#titre_image');	
	}
	else
	{
		$_SESSION['erreur_gestion_image_admin'] = "Vous avez entré plus de 38 caractères dans le champs 'commentaire'.";
		header('location: ../gerer_foto.php#titre_image');
	}
}
else
{
		$_SESSION['erreur_gestion_image_admin'] = "Une erreur s'est produit.";
		header('location: ../gerer_foto.php#titre_image');
}
														
														
?>														