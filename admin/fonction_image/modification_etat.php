<?php
session_start();
include ('../verification/verification_acces.php');
if(isset($_GET['id']) && !empty($_GET['id']) && $_GET['etat'])
{
	require '../include/mysql.inc.php';

	switch($_GET['etat'])
	{
		case 'active' :
			$req = $bdd->query("SELECT count(id) as total_image FROM images WHERE id_categorie_image= 1 && active= 1") or die(print_r($bdd->errorInfo()));
			$total= $req->fetch();
			$total_photo_active = $total['total_image'];
			if($total_photo_active <18)
			{
				include_once('Redimensionner.class.php');
				$req = $bdd->query("SELECT id,url,active	FROM images WHERE id=".$_GET['id']."") or die(print_r($bdd->errorInfo()));
				$donnee = $req->fetch();
				$redimensionner = new Redimensionner('../../'.$donnee['url'] ,'../../'.$donnee['url']);
				$redimensionner->redimensionner();
				unset($redimensionner);
				$bdd->exec("UPDATE images SET active = 1 WHERE id=".$_GET['id']."");
				$_SESSION['info_gestion_image_admin'] = "L'image a bien été activée.";
			}
			else
			{
				$_SESSION['erreur_gestion_image_admin'] = "Vous ne pouvez pas activer plus de 18 photos.";
			}
				header('location: ../gerer_foto.php#titre_image');
		break;
		case 'desactive':
			$bdd->exec("UPDATE images SET active = 0 WHERE id=".$_GET['id']."");
			$_SESSION['info_gestion_image_admin'] = "L'image a bien été désactivée.";
			header('location: ../gerer_foto.php#titre_image');
		break;
		case 'supprime':
			$req = $bdd->query("SELECT id,url,active	FROM images WHERE id=".$_GET['id']."") or die(print_r($bdd->errorInfo()));
			$donnee = $req->fetch();
			$urlfichier = '../../'.$donnee['url'];
			if (file_exists($urlfichier))		//on vérifie que l'image existe bien
			{
				unlink($urlfichier);
			}
			$bdd->exec("DELETE FROM images WHERE id=".$_GET['id']."");
			$_SESSION['info_gestion_image_admin'] = "Votre image a bien été supprimée.";
			header('location: ../gerer_foto.php#titre_image');
		break;
		default:
			$_SESSION['erreur_gestion_image_admin'] = "Une erreur s'est produite!!!!!!";
			header('location: ../gerer_foto.php#titre_image');
		break;
	}
}
?>