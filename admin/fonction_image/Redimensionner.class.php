<?php
class Redimensionner
{
	private $chemin;
	private $largeur_image;
	private $hauteur_image;
	private $qualite;
	private $color;
	private $destination;

	public function __construct($nouveauChemain,$nouvelleDestination)
	{
			$this->chemin = $nouveauChemain;
			$this->destination = $nouvelleDestination;
			$this->largeur_image = 300; // Taille de l'image
			$this->hauteur_image = 300;
			$this->qualite = 80; // Qualite de l'image (0=pourrit/100=super)
			$this->color = "000000"; // Couleur de fond	
	}
		public function setTaile($nouvelleLargeur,$nouvelleHauteur)
	{
		$this->largeur_image = $nouvelleLargeur;
		$this->hauteur_image = $nouvelleHauteur;
	}
		public function setqualite($nouvellequalite)
	{
		$this->qualite = $nouvellequalite;
	}
		public function redimensionner()
	{
			if (file_exists($this->chemin))		//on vérifie que l'image existe bien
			{
				$size = getimagesize($this->chemin);		// récupération des dimension de l'image
				
				if($size[0] != $this->largeur_image AND $size[1] != $this->hauteur_image)	//on vérifie que l'on ne dispose pas déjà d'une image = la largeur demander et = la hauteur 		
				{																																	//demander
					if($size[0] >= $this->largeur_image AND $size[1] >= $this->hauteur_image) 	//si la taille (hauteur et largeur) de l'image est plus grand que l'image créer
					{
						if(($size[0]/$this->largeur_image) > ($size[1]/$this->hauteur_image)) 	
						{
							$x_t = $this->largeur_image;
							$y_t = floor(($size[1]*$this->largeur_image)/$size[0]);
							$x_p = 0;
							$y_p = ($this->hauteur_image/2)-($y_t/2);
						} 
						else 
						{
							$x_t = floor(($size[0]*$this->hauteur_image)/$size[1]);
							$y_t = $this->hauteur_image;
							$x_p = ($this->largeur_image/2)-($x_t/2);
							$y_p = 0;
						}
					} 
					else 
					{
						$x_t = $size[0];
						$y_t = $size[1];
						$x_p = ($this->largeur_image/2)-($x_t/2);
						$y_p = ($this->hauteur_image/2)-($y_t/2);
					}

					$extension = strrchr($this->chemin,'.');
					$extension = strtolower(substr($extension,1));
					if($extension == 'jpg' OR $extension == 'jpeg') 
					{	
						$image_new = imagecreatefromjpeg($this->chemin);
					} 
					elseif($extension == 'gif') 
					{
						$image_new = imagecreatefromgif($this->chemin);
					} 
					elseif($extension == 'png') 
					{
						$image_new = imagecreatefrompng($this->chemin);
					} 
					elseif($extension == 'bmp') 
					{
						$image_new = imagecreatefromwbmp($this->chemin);
					} 
					else 
					{
						echo "Erreur !";
						exit;
					}
					$image = imagecreatetruecolor($this->largeur_image, $this->hauteur_image);
					$color = imagecolorallocate($image, hexdec($this->color[0].$this->color[1]), hexdec($this->color[2].$this->color[3]), hexdec($this->color[4].$this->color[5]));
					imagefilledrectangle($image,0,0,$this->largeur_image,$this->hauteur_image,$this->color);
					imagecopyresampled($image,$image_new,$x_p,$y_p,0,0,$x_t,$y_t,$size[0],$size[1]);
					imagejpeg($image,$this->destination,$this->qualite);
					imagedestroy($image);
				}
			}
		}
		
}
?>