<!-----
réalialiser par: kevin detrrain,
but: projet TFE
année: 2014
utilisé:
Cette page permet  la gestion des annonces.
l'affichage des annonces se fait dans les fichiers:
->include/news.php
->admin/include/news.php

ajout/suppression/modfification
->admin/anonce_post.php
------>
<?php
session_start();
include ('verification/verification_acces.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if gt IE 7]>
			<link rel="stylesheet" href="../style_ie.css" />
		<![endif]-->
		<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php

			include ('include/header.php');
			include ('include/bar_de_menu.php');

		?>
		<div id="conteneur_principal">
			<div id="zone_affichage">
				<div>
					<h1 style=" margin-top:0px;">Gestion des utilisateurs</h1>
					<!-------cadre d'ajout d'utilisateurs----------->
						<form method='post' action='utilisateur_post.php'>
						 <fieldset>
						<legend>Ajouter un utilisateur</legend>
						<table>
						<tr><td>Identifiant :</td><td> <input  type="text" style="width:100px;" required="required" name="id"  placeholder=' Identifiant'/></td>
						<tr><td>Nom :</td><td> <input  type="text" style="width:100px;" required="required" name="nom"  placeholder=' Nom'/></td>
						<tr><td>Prenom :</td><td> <input  type="text" style="width:100px;" required="required" name="prenom"  placeholder=' Prenom'/></td>
						<tr><td>Mot de passe :</td><td> <input  type="password" style="width:100px;" required="required" name="mdp"  placeholder=' Mot de passe'/></td>
						<tr><td>type de compte : </td><td><select  name="compte"><option value="visiteur">Visiteur</option><option value="administrateur">Administrateur</option></select></td>
						<input type='hidden' name='categorie' value='ajout'>
						<tr><td colspan="2" align="right"><input type='submit' value='Ajouter' /></td>
						 </table>
						 </fieldset>
						</legend>
					</form>
					<!----------------------------------------------------->
				</div>
				<div>
				<table border align="center" style="margin-top:20px;">
				<tr><th>Identifiant</th><th>Nom</th><th>Prenom</th><th>Mot de passe</th><th>Type de compte</th></tr>
				<?php
				require 'include/mysql.inc.php';
				$utilisateurs_req = $bdd->query('SELECT * FROM clients ORDER BY id DESC');
				/*------------boucle d'affichage des annonce-------------------------*/		
				
					while($utilisateurs = $utilisateurs_req->fetch())
					{
					echo '<tr><td>'.$utilisateurs['id'].'</td><td>'.$utilisateurs['nom'].'</td><td>'.$utilisateurs['prenom'].'</td><td>'.$utilisateurs['mot_de_passe'].'</td><td>'.$utilisateurs['compte'].'</td>';
					}
				/*--------------------fin de la boucle-------------------------------------*/		
				?>
				</table>
				</div>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
	<script type="text/javascript" src="js/monJQ.js"></script>
	</body>
</html >
