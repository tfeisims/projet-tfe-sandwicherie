		<?php		require_once 'include/mysql.inc.php';
								$req = $bdd->query("SELECT  MIN(date_cree) as min_date  FROM commandes ") 	or die(print_r($bdd->errorInfo()));
								$donnee= $req->fetch();
								$date_min = date("Y,m,d", strtotime($donnee['min_date']));
	?>


	$(document).ready(function() {

        var weekDays = AddWeekDays(3);
        var natDays = [
          [1, 1, 'fr',2014],
		  [3,27,'fr',2014],
          [12, 25, 'fr',2014],
          [12, 26, 'fr',2014]
        ];

        function noWeekendsOrHolidays(date) {
            var noWeekend = $.datepicker.noWeekends(date);
            if (noWeekend[0]) {
                return nationalDays(date);
            } else {
                return noWeekend;
            }
        }
        function nationalDays(date) {
            for (i = 0; i < natDays.length; i++) {
                if (date.getFullYear() == natDays[i][3] && date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1]) {
                    return [false, natDays[i][2] + '_day'];
                }
            }
            return [true, ''];
        }
        function AddWeekDays(weekDaysToAdd) {
            var daysToAdd = 0
            var mydate = new Date()
            var day = mydate.getDay()
            weekDaysToAdd = weekDaysToAdd - (5 - day)
            if ((5 - day) < weekDaysToAdd || weekDaysToAdd == 1) {
                daysToAdd = (5 - day) + 2 + daysToAdd
            } else { // (5-day) >= weekDaysToAdd
                daysToAdd = (5 - day) + daysToAdd
            }
            while (weekDaysToAdd != 0) {
                var week = weekDaysToAdd - 5
                if (week > 0) {
                    daysToAdd = 7 + daysToAdd
                    weekDaysToAdd = weekDaysToAdd - 5
                } else { // week < 0
                    daysToAdd = (5 + week) + daysToAdd
                    weekDaysToAdd = weekDaysToAdd - (5 + week)
                }
            }

            return daysToAdd;
        }	
    $( ".datepicker" ).datepicker({
		constrainInput: true,   // prevent letters in the input field
		minDate: new Date(),    // prevent selection of date older than today
	   // showOn: 'button',       // Show a button next to the text-field
		autoSize: true,         // automatically resize the input field 
		dateFormat: "dd/mm/yy",  // Date Format used
		beforeShowDay: noWeekendsOrHolidays,     // Disable selection of weekends
		firstDay: 1, // Start with Monday
		showOn: 'both', buttonImageOnly: true, 
		buttonImage: 'css/images/calendar.png'  
});
	$( ".datepicker" ).datepicker( $.datepicker.regional[ "fr" ] );

  });		