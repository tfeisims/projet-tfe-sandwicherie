<?php
session_cache_limiter('private_no_expire');
session_start();

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>	
		<!--[if gt IE 8]>
			<link rel="stylesheet" href="../style_ie.css" />
        <![endif]-->
	<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');		
		?>	
		<div id="conteneur_principal">
			<!-----zone central contenant les élément important---------------------->
			<div id="zone_affichage">
			<?php 
			if(isset($_GET['id_produit']))
			{
				// On se connecte à  MySQL
				require 'include/mysql.inc.php';
				$id_produit = $_GET['id_produit'];
				
				$req = $bdd->query("SELECT  id,nom,prix,disponible,date_ajout,description  FROM produits where id=".$id_produit."") 	or die(print_r($bdd->errorInfo()));
				$donnee= $req->fetch();
							$date_ajout = date("d/m/Y ", strtotime($donnee["date_ajout"]));
				
				echo '<h1>Détails du produit "'.$donnee["nom"].'"</h1>';
			?>			
			<div style="border:1px solid black; width:600px; height:500px;margin-left:10px;border-radius: 10px;">
				<div>
					<img src="../img/defaut_carousel.png" alt="image_du_produit" style="width:150px;height:150px;border-radius: 10px; margin:10px;"/>
					<form style="display:inline-block;vertical-align:top;margin-top: 15px; vertical-align: top; padding: 6px;background-color: rgb(53, 129, 226);border-radius: 5px;" method="post" action="cible_envoye.php" enctype="multipart/form-data">
						<input type="file" style="background-color: rgb(224, 224, 224); padding: 3px;border-radius: 5px;" name="monfichier"/><br/>
						<input type="submit" style="border: 1px solid black;  margin-left: 270px;margin-top: 5px;" value="Changer la photo"/>
					</form>
				</div>
				<div style="margin-left:15px;">
					
					<div style="padding:3px;"><span>Nom du produit : </span><?php echo $donnee["nom"];?></div>
					<div style="padding:3px;"><span>Prix : </span><?php echo $donnee["prix"];?> €</div>
					<div style="padding:3px;"><span>Disponibilité :</span><?php if($donnee["disponible"] == 0){echo ' oui';}else{echo ' non';}?></div>				
					<div style="padding:3px;"><span>Ajouté le :</span><?php echo $date_ajout;?></div>
					<form method="post" action="#">
					Description:<br/>
					<textarea style="width: 550px; height: 137px;resize:none;"><?php echo $donnee["description"]; ?></textarea>
					<input type="submit" style="    margin-left: 480px; margin-top: 10px; "value="Modifier"/>
					</form>
				</div>
			
			
			</div>
	
			
			<?php
			}
			else
			{
				echo 'Aucun produit n\'a été trouvé.';			
			}
			?>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script type="text/javascript" src="js/monJQ.js"></script>	
		<!--<script  src="js/monDatepicker.js"></script>-->

	</body>
</html>