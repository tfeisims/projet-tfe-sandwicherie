﻿<?php
session_start();
include ('verification/verification_acces.php');

if(isset($_GET['id_produit']))
{
	$id_produit = $_GET['id_produit'];

	require 'include/mysql.inc.php';
	$total_req= $bdd->query("SELECT COUNT(id) as total_annonce,active FROM news where active=1");
	$total_annonce= $total_req->fetch();
	if($total_annonce['total_annonce'] < 4)
	{
		$req = $bdd->query('SELECT p.id, p.nom as nom_produit, p.id_categorie, c.id, c.nom as nom_categorie 
		FROM produits p 
		INNER JOIN categorie c
		ON c.id = p.id_categorie
		WHERE p.id='.$id_produit.'');
		$donnee = $req->fetch();

		$message = 'Le produit "'.$donnee['nom_produit'].'" a été ajouté dans la catégorie "'.$donnee['nom_categorie'].'".';
		$req->CloseCursor();
		
		$bdd->exec("INSERT INTO news (message, active,date_cree) VALUES('".$message."',1,NOW())");
		
		$_SESSION['info_commander_admin'] = "Une annonce a été publier.";
			header('location: commander_admin.php');
	}
	else
	{
		$_SESSION['erreur_commander_admin'] = "Le nombre maximum d'annonces est déjà atteint.";
		header('location: commander_admin.php');	
	}
}
else
{
	$_SESSION['erreur_commander_admin'] = "Une erreur s'est produit lors de la publication.";
	header('location: commander_admin.php');
}
?>