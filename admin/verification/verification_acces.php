<?php
if(!isset($_SESSION['compte']) && $_SESSION['compte'] != 'administrateur')
{
	if(file_exists("../erreur.php"))
	{
		header('Location: ../erreur.php');
	}
	else
	{
		header('Location: ../../erreur.php');
	}
}
?>