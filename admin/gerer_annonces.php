<!-----
réalialiser par: kevin detrrain,
but: projet TFE
année: 2014
utilisé:
Cette page permet  la gestion des annonces.
l'affichage des annonces se fait dans les fichiers:
->include/news.php
->admin/include/news.php

ajout/suppression/modfification
->admin/anonce_post.php
------>
<?php
session_start();
include ('verification/verification_acces.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="../style_ie.css" />
		<![endif]-->
		<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
			require 'include/mysql.inc.php';
			include ('include/header.php');
			include ('include/bar_de_menu.php');
			$nombre_annonces_req = $bdd->query('SELECT COUNT(*) as total FROM news WHERE active=1 ORDER BY id DESC');
			$nombre_annonces = $nombre_annonces_req->fetch();
		?>
		<div id="conteneur_principal">
			<div id="zone_affichage">
				<div>
					<h1 style=" margin-top:0px;">Gestion des annonces</h1>
					<!-------cadre d'ajout d'annonce----------->
						<form method='post' action='annonce_post.php'>
						 <fieldset>
						<legend>Ajouter une annonce</legend>
						<input  type="text" style="width:520px;" name="annonce" <?php if($nombre_annonces['total'] >= 4){ echo 'disabled';}?> maxlength="150" placeholder=' Faites une annonces de maximum 150 caractères'/>
						<input type='hidden' name='categorie' value='ajout'>
						<input type='submit' <?php if($nombre_annonces['total'] >= 4){ echo 'disabled';}?> value='Ajouter' />
						 </fieldset>
						</legend>
					</form>
					<!----------------------------------------------------->
				</div>
				<div>
				<?php
						$req_annonces = $bdd->query('SELECT * FROM news WHERE active=1 ORDER BY id DESC');
						$compteur_edit=1;
				/*------------boucle d'affichage des annonce-------------------------*/		
						while($annonces = $req_annonces->fetch())
						{
							//$annonce = wordwrap($annonces['message'], 80, "<br />",true);
							$date_annonce = date("d/m/Y H:m:s", strtotime($annonces['date_cree']));
							$date_modif = date("d/m/Y H:m:s", strtotime($annonces['date_modif']));
							
							echo '<div style="width:600px; border: 1px solid black; margin-left:10px; margin-top:20px; padding: 5px 5px 0px 5px;">';
							echo '<h2 style="margin-top:0px; margin-bottom:0px;font-size:18px;position:relative; top:-17px; padding-left:2px;padding-right:2px; left:5px;background-color: rgb(179, 179, 179);display:inline;">Annonce n°'.$annonces['id'].'</h2>
							<span style="float:right;"><img class="b_editer_admin" id="edit'.$compteur_edit.'" onClick="affiche_form(this);" alt="img_edit" title="Modifier" style="width:14px;" onmouseout="mouse_out_img(this);" onmouseover= "mouse_over_img(this);" src="../img/editer.png"/><a href="annonce_post.php?id='.$annonces['id'].'&&categorie=supp"><img class="b_supprimer_admin" alt="img_edit" title="Supprimer" style="width:14px;" onmouseout="mouse_out_img(this);" onmouseover= "mouse_over_img(this);" src="../img/corbeille.png"/></a></span>
							<p id="paragra'.$compteur_edit.'"style="width:600px;text-align:justify;word-wrap: break-word;margin-top:0px; margin-bottom:0px;color:rgb(0,35,124);">'.$annonces['message'].'</p>
							<form method="post" action="annonce_post.php"  id="formulaire'.$compteur_edit.'" style="display:none;margin-bottom:10px;">
							<TEXTAREA style="width:590px;height:60px;resize:none;background-color:rgb(200,200,200);" name="annonce" maxlength="150"  placeholder= "Faites une annonces de maximum 150 caractères">'.$annonces['message'].'</TEXTAREA>
							<input type="hidden" name="categorie" value="modifier">
							<input type="hidden" name="id" value="'.$annonces['id'].'">
							<input style="margin-left:523px;" type="submit" value="Modifier" />
							</form>
							<br/><span style="float:right;font-size:12px;color: rgb(0,9,114);';if($date_modif > $date_annonce){echo 'position:relative;top:-12px;left:0px;';} echo '">Créé le '.$date_annonce.'<br/>';
							if($date_modif > $date_annonce)
							echo 'Modifié le '.$date_modif.'';
							echo '</span><br/></div>';
							$compteur_edit++;
						}
				/*--------------------fin de la boucle-------------------------------------*/		
				?>
				</div>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
	<script type="text/javascript" src="js/monJQ.js"></script>
	<script>
	<!---------------formulaire permettant l'affichage ou non du cadre permettant de modifier le commentaire
			function affiche_form(elm)
			{
				<?php
					for($i=1;$i <=$compteur_edit;$i++)
					{
							
						echo "if(elm.id== 'edit".$i."'){	
									if(document.getElementById('paragra".$i."').style.display != 'none')
									{
										document.getElementById('paragra".$i."').style.display = 'none';
										document.getElementById('formulaire".$i."').style.display = 'block';
									}
									else
									{
										document.getElementById('paragra".$i."').style.display = 'block';
										document.getElementById('formulaire".$i."').style.display = 'none';
									}
									}";
										
					
					}
				
				?>
			}
		
	</script>
	</body>
</html >
