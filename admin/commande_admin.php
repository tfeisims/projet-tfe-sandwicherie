<?php
session_cache_limiter('private_no_expire');
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="css/chosen.min.css">		
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />		
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script src="js/chosen.jquery.min.js" type="text/javascript"></script>		
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!---javascript pour le DatePicker------------------>
	<link href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet">
	<script src="js/jquery-ui-1.9.2.custom.js"></script>
	<script  src="js/jquery.ui.datepicker-fr.js"></script>	
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="../style_ie.css" />
        <![endif]-->
	<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
				
				// On se connecte à  MySQL
				require 'include/mysql.inc.php';
				$req = $bdd->query("SELECT  MAX(date_cree) as max_date  FROM commandes where confirmation=1") 	or die(print_r($bdd->errorInfo()));
				$donnee= $req->fetch();
				$date_dernier_comm = date("d/m/Y", strtotime($donnee['max_date']));

				//récupérer le jour actuel
				setlocale(LC_TIME, 'french');
				//variables pour récupérer les commandes réalisé
				$heure_du_jour_ouverture= date("Y/m/d 7:00:00", strtotime($donnee['max_date']));
				$heure_du_jour_fermeture=date("Y-m-d 23:00:00", strtotime($donnee['max_date']));
				//variable pour afficher ou non les actions sur les commandes
				$journee_afficher = date("Y-m-d", strtotime($donnee['max_date']));
				$d_afficher = date("d/m/Y", strtotime($donnee['max_date']));		//date pour le pdf
				// rédéfinition du jours de la commande par celui que le client a choisi
				if(isset($_POST['date_commande']))
				{
					$date_ouverture = explode("/", $_POST['date_commande'] );
					$date_affiche_commande = ''.$date_ouverture[0].'-'.$date_ouverture[1].'-'.$date_ouverture[2].'';
					$heure_du_jour_ouverture=date( 'Y-m-d 7:00:00', strtotime($date_affiche_commande));
					$heure_du_jour_fermeture=date( 'Y-m-d 23:00:00', strtotime($date_affiche_commande));
					$journee_afficher = date('Y-m-d', strtotime($date_affiche_commande));	
			}

				if(isset($_POST['utilisateur']))
				{					
					//requete pour récupérer le nombre total de commande/après la recherche d'un utilisateur
					$req_total_commande = $bdd->query("SELECT COUNT(DISTINCT id_client) as total_commande,date_cree, confirmation	FROM commandes WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1' && id_client LIKE '%".$_POST['utilisateur']."%'
					") or die(print_r($bdd->errorInfo()));
					$total = $req_total_commande->fetch();
					$total_commande = $total['total_commande'];
				
				}
				else
				{
					//requete pour récupérer le nombre total de commande
					$req_total_commande = $bdd->query("SELECT COUNT(DISTINCT id_client) as total_commande,date_cree, confirmation	FROM commandes WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1'
					") or die(print_r($bdd->errorInfo()));
					$total = $req_total_commande->fetch();
					$total_commande = $total['total_commande'];
				}
			
				?>	
		<div id="conteneur_principal">
			<!-----zone central contenant les élément important---------------------->
			<div id="zone_affichage">
			<h1>Gestion des commandes</h1>
			<div style="border:1px solid black;padding-top:10px; padding-bottom:10px;border-radius: 10px; padding-left:2px;">
			<!-----------Formulaire pour la selection de la période des commandes a afficher----->
			<form method="post" action="commande_admin.php" style="margin-bottom:10px;">
					Obtenir les commandes du
					<input type="text" class="datepicker" name="date_commande" value="<?php if(isset($_POST['date_commande'])){ echo $_POST['date_commande'];} else{ echo $date_dernier_comm;}?>">
					<input type="submit" value="Afficher"/>
				<?php
				if($total_commande > 0)
				{
					echo '<a class="lien_detail" href="tcpdf/genere_pdf.php?d_commande='; if(isset($_POST['date_commande'])){echo $_POST['date_commande'];}else{ echo $d_afficher;} echo '" target="_blank" style="font-size:10px;">(Générer un PDF)</a>';
				}
				else
				{
					echo '<span style="font-size:10px;">(Il n\'y a pas de pdf disponible)</span>';
				}
				?>
				</form>
				<!-----------Rechercher un utilisateur dans la période selectionner----->
				<form method="post" action="commande_admin.php" >
					Trouver un client : 
					<input type="texte" name="utilisateur" placeholder="Entrer un nom" value="<?php if(isset($_POST['utilisateur'])){ echo $_POST['utilisateur'];} ?>"/>
					<input type="hidden" name="date_commande" value="<?php if(isset($_POST['date_commande'])){ echo $_POST['date_commande'];} else{ echo $date_dernier_comm;}?>"/>
					<input type="submit" value="Rechercher"/>
					<?php  if(isset($_POST['utilisateur'])){ echo ' '.$total_commande.' correspondance(s) a/ont été trouvée(s).'; }?>				
				</form>
				Afficher : <span class="bouton_modifier" id="bouton_toute_commande" onClick="modif_bouton(this);">Toutes les commandes</span> / <span onClick="modif_bouton(this);" id="bouton_recapitulative" class="bouton_classique">Tableau récapitulatif</span>
			</div>
			<?php
			if(isset($_POST['date_commande'])){echo '<h2 id="titre_commande">Commande du '.$_POST['date_commande'].'</h2>';}
			else{
			echo '<h2 id="titre_commande">Commande du '.$date_dernier_comm.' </h2>';}
			
					if($total_commande > 0)
					{
						if(isset($_POST['utilisateur']))
						{
							//----------recherche d'un utilisateur dans le lapse de temps défini juste avant
							$req = $bdd->query("SELECT c.id_client as id_client, c.quantite as quantite_produit, c.date_cree as date_cree, c.confirmation as confirmation, c.id_produit, c.commentaire as commentaire,p.id, p.nom as produit, p.prix as prix_produit	
							FROM commandes c
							INNER JOIN produits p
							ON p.id = c.id_produit
							WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1' && id_client LIKE '%".$_POST['utilisateur']."%'
							ORDER BY id_client") 
							or die(print_r($bdd->errorInfo()));
						}
						else
						{
						//----------récupération des commandes
							$req = $bdd->query("SELECT c.id_client as id_client, c.quantite as quantite_produit, c.date_cree as date_cree, c.confirmation as confirmation, c.id_produit, c.commentaire as commentaire,p.id, p.nom as produit, p.prix as prix_produit	
							FROM commandes c
							INNER JOIN produits p
							ON p.id = c.id_produit
							WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1'
							ORDER BY id_client") 
							or die(print_r($bdd->errorInfo()));
						}
						$total_a_payer=0;
						$id = null;
						$commentaire = null;
							//partie permettant de créer le tableau contenant le panier de l'utilisateur
								while($commande = $req->fetch())
								{	
									if($id != $commande['id_client'])
									{
										if($id != null)	// permet d'avoir une fin de tableau lors qu'il y a plusieurs client 
										{
									$liste_produit_req = $bdd->query("SELECT p.id as id_produit,p.nom as nom_produit,p.id_categorie, c.id as id_categorie,c.nom as nom_categorie
												From produits p
												INNER JOIN categorie c
												ON p.id_categorie = c.id
												WHERE disponible=1 && supprimer=0
												ORDER BY p.id_categorie,p.id
												"); 
								if($journee_afficher == date("Y-m-d"))
								{		
									echo '<form class="ajout_produit_commande" method="post" action="commande_post.php" style=" border-color: black; border-style: solid;   border-width: 0px 1px 1px 1px;">
												<span>Ajouter un produit: </span>
												<div class="zone_ajout_produit_commande" style="display:inline;">';
											//------------------------------------------auto-competion	
											echo '<select data-placeholder="Liste des produits" style="width:340px;" class="chosen-select" name="nom_produit">';
												$nom_categorie_tmp = null;
												while($liste_produit=$liste_produit_req->fetch())
												{
													if($liste_produit['nom_categorie'] !=$nom_categorie_tmp)
													{
													if($nom_categorie_tmp != null)
													echo "</optgroup>";
													echo "<optgroup label='".$liste_produit['nom_categorie']."'>";
													 $nom_categorie_tmp = $liste_produit['nom_categorie'];
													}
													echo "<option>".$liste_produit['id_produit'].'. '.$liste_produit['nom_produit']."</option>";
												}
												echo' </optgroup>
											</select>';
											//--------------------------------------------fin autcompletion-----------------------------------------------------------------------------
									echo '</div>
												<span>Quantité: </span>
												<div class="zone_quantite_commande" style="display:inline;"><input type="hidden" name="id_client" value="'.$id.'"/><input type="number" required="required" name="quantite" class="entree_formulaire_panier"/></div>
												<div class="zone_validation_commande" style="display:inline;"><input type="submit" value="Ajouter"/></div>
											</form>';
									}		
											echo '<div class="cadre_total_panier">';
											echo '<div class="champs_vide">Total à payer : </div>';
											echo	'<div class="col_somme_total">'.$total_a_payer.'€</div>';
											echo '</div>';
											echo '</div>'; 
										}
										$total_a_payer = 0;
										$commentaire = $commande['commentaire'];
										$id = $commande['id_client'];
										echo "<span class='detail_tab_commande' style='display:block;'><b>Client: ".$id."</b></span>";		//nom du client qui a passer la commande
										echo "<span class='detail_tab_commande' style='display:block;'><b>Commentaire:</b> ".$commentaire."</span>";
										echo '<div class=tableau_panier>
															<div class="titre_tableau_panier">
																<div class="titre_produit_tableau_commande">N° de produit</div>
																<div class="titre_PU_tableau">Prix unitaire</div>
																<div class="titre_quantite_tableau">Quantité</div>
																<div class="titre_total_tableau" >Total</div>
																<div class="titre_actions_tableau">Actions</div>
															</div>';
													$i = 0;
										}
			
													$quantite = $commande['quantite_produit'];
													$produit = $commande['produit'];
													$id_produit = $commande['id_produit'];
													$prix = $commande['prix_produit'];
													$total = $quantite * $prix; 
													
													echo "<div class='champs_produit ";
													if($i%2 == 0)
														echo "font_clair'>";
													else
														echo "font_foncer'>";
														
														echo "<form method='post' action='commande_post.php'>";
														echo '<div class="col_nom_produit_commande">' .$produit .'</div>';
														echo '<div class="col_prix_produit">'.$prix.'€</div>';
														echo '<div class="col_quantite_produit">';
														if($prix != 0)	//si le produits cout 0 € on  demande au client si il en veut ou pas
														{
															echo '<input  type="number"'; if($journee_afficher != date("Y-m-d")){echo ' disabled ';}	echo 'class="entree_formulaire_panier" name="quantite" value="' . $quantite . '"/>';
														}
														else
														{
															echo "<select  name='quantite' "; if($journee_afficher != date("Y-m-d")){echo ' disabled ';}	echo " class='entree_formulaire_panier_gratuit' disabled='disabled'><option value='0'>Non</option><option value='1' selected='selected'>Oui</option></select>";
														}
														echo '</div>';
														echo '<div class="col_total_produit">'.$total.'€</div>';
														echo '<div class="col_action_produit">';
														if($journee_afficher == date("Y-m-d"))
														{														
														echo '<input type="hidden" name="id_client" value="' . $id .'"/><input type="hidden" name="id_produit" value="' . $id_produit .'"/><input type=image Value=submit class="b_editer" title="Modifier" src="../img/editer.png" onmouseOver="mouse_over_img(this);" onmouseOut="mouse_out_img(this);"/><a href="commande_post.php?id_client='.$id.'&id_produit='.$id_produit.'" title="Supprimer"><img src="../img/corbeille.png" class="b_supprimer" BORDER="0" alt="img edit" onmouseOver="mouse_over_img(this);" onmouseOut="mouse_out_img(this);"/></a>';
														}
														else
														{
														echo 'Non disponible';
														}
														echo '</div></div>';
														echo '</form>';
													$total_a_payer += $total;
													$i++;
								}		
								if($id != null) //on vérfie qu'il y a au moins une ligne de produit
								{
									$liste_produit_req = $bdd->query("SELECT p.id as id_produit,p.nom as nom_produit,p.id_categorie, c.id as id_categorie,c.nom as nom_categorie
																							From produits p
																							INNER JOIN categorie c
																							ON p.id_categorie = c.id
																							WHERE disponible=1 && supprimer=0
																							ORDER BY p.id_categorie,p.id
																							"); 
									
									// fermeture du dernier tableau
							if($journee_afficher == date("Y-m-d"))
							{	
									echo '<form class="ajout_produit_commande" method="post" action="commande_post.php" style=" border-color: black; border-style: solid;   border-width: 0px 1px 1px 1px;">
										<span>Ajouter un produit: </span>
										<div class="zone_ajout_produit_commande" style="display:inline;">';
									//------------------------------------------auto-competion	
									echo '<select data-placeholder="Liste des produits" style="width:340px;" class="chosen-select" name="nom_produit">';
										$nom_categorie_tmp = null;
										while($liste_produit=$liste_produit_req->fetch())
										{
											if($liste_produit['nom_categorie'] !=$nom_categorie_tmp)
											{
											if($nom_categorie_tmp != null)
											echo "</optgroup>";
											echo "<optgroup label='".$liste_produit['nom_categorie']."'>";
											 $nom_categorie_tmp = $liste_produit['nom_categorie'];
											}
											echo "<option>".$liste_produit['id_produit'].'. '.$liste_produit['nom_produit']."</option>";
										}
										echo' </optgroup>
									</select>';
									//--------------------------------------------fin autcompletion-----------------------------------------------------------------------------
							echo '</div>
										<span>Quantité: </span>
										<div class="zone_quantite_commande" style="display:inline;"><input type="hidden" name="id_client" value="'.$id.'"/><input type="number" required="required" name="quantite" class="entree_formulaire_panier"/></div>
										<div class="zone_validation_commande" style="display:inline;"><input type="submit" value="Ajouter"/></div>
									</form>';
							}
									echo '<div class="cadre_total_panier">';
									echo '<div class="champs_vide">Total à payer : </div>';
									echo	'<div class="col_somme_total">'.$total_a_payer.'€</div>';
									echo '</div>';
									echo '</div>';
								}
								//tableau récapitulative
								$commande_req = $bdd->query("SELECT c.id_produit, SUM(c.quantite) AS quantite_total,c.date_cree,c.confirmation,p.id, p.nom as produit, p.prix
										FROM commandes c
										INNER JOIN produits p
										ON p.id = c.id_produit
										WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1'
										GROUP BY id_produit
										")
										or die(print_r($bdd->errorInfo()));
							?>
							<h2 id="titre_tab_recapitulative" style="display:none;">Tableau récapitulative</h2>
					<table Border CELLPADDING="2" CELLSPACING="2" id="tab_recapitulative" WIDTH="100%" style="text-align:center;display:none;">
						<tr>
							<th>Produit</th>
							<th>Prix</th>
							<th>Quantité totale</th>
							<th>Somme totale</th>
						</tr>
					<?php 
					$somme_total = 0;
						while($commande_general = $commande_req->fetch())
								{
									$total = 0;
									$total= $commande_general['prix'] * $commande_general['quantite_total'];
								echo '<tr>';	
									echo '<td>'.$commande_general['produit'].'</td>
											<td>'.$commande_general['prix'].'€</td>
											<td>'.$commande_general['quantite_total'].'</td>
											<td> '.$total.'€</td>';
								echo '</tr>';
								$somme_total = $somme_total + $total;
								}
			?>	
					<tr>
						<td colspan="3"  style="text-align:right; padding-right:10px;"> TOTAL:</td>
						<td><?php echo $somme_total.'€'; ?></td>
					</tr>
					</table>
						<?php	
							}
							else
							{
								if(isset($_POST['utilisateur']))
								{					
									echo "Aucun client ne correspond à votre recherche.";
								}
								else
								{
									echo "Aucun client n'a réalisé de commande à ce moment.";
								}
							}
									
							?>				
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script type="text/javascript" src="js/monJQ.js"></script>	
		<!--<script  src="js/monDatepicker.js"></script>-->
<script>
		<?php
								$req = $bdd->query("SELECT  MIN(date_cree) as min_date, MAX(date_cree) as max_date  FROM commandes WHERE confirmation=1") 	or die(print_r($bdd->errorInfo()));
								$donnee= $req->fetch();
								$date_min = date("Y,m-1,d", strtotime($donnee['min_date']));
								$date_max = date("Y,m-1,d", strtotime($donnee['max_date']));
	?>


	$(document).ready(function() {
	
    $( ".datepicker" ).datepicker({
		constrainInput: true,   // prevent letters in the input field
		minDate: new Date(<?php echo $date_min; ?>),    // prevent selection of date older than today
		maxDate: new Date(<?php echo $date_max; ?>),    // prevent selection of date older than today
	   // showOn: 'button',       // Show a button next to the text-field
		autoSize: true,         // automatically resize the input field 
		dateFormat: "dd/mm/yy",  // Date Format used
		beforeShowDay: $.datepicker.noWeekends,     // Disable selection of weekends
		firstDay: 1, // Start with Monday
		showOn: 'both', buttonImageOnly: true, 
		buttonImage: 'css/images/calendar.png'  
});
	$( ".datepicker" ).datepicker( $.datepicker.regional[ "fr" ] );

  });

function modif_bouton(elm) 
{
/*Afficher : <span class="bouton_modifier" id="bouton_toute_commande" onClick="modif_bouton(this);">Toutes les commandes</span> / <span onClick="modif_bouton(this);" id="bouton_recapitulative" class="bouton_classique">Tableau récapitulative</span>*/
	if(elm.id == "bouton_toute_commande" && elm.className == "bouton_classique")
	{	
	// mettre bouton modifier pour bouton toute commande
		elm.className= "bouton_modifier"; 	
		var btn = document.getElementById("bouton_recapitulative"); 
		btn.className= "bouton_classique";
	
	document.getElementById("titre_tab_recapitulative").style.display= 'none';			
	document.getElementById("tab_recapitulative").style.display= 'none';
	document.getElementById("titre_commande").style.display= 'block';		
	
		var tab_commande = document.getElementsByClassName('tableau_panier');	
		for(var i = 0; i < tab_commande.length; i++)
		 {
			tab_commande[i].style.display = 'block';
		}
	
		var tab_detail = document.getElementsByClassName('detail_tab_commande');	
		for(var i = 0; i < tab_detail.length; i++)
		 {
			tab_detail[i].style.display = 'block';
		}
	
	}
	else if(elm.id == "bouton_recapitulative" && elm.className == "bouton_classique")
	{
	// mettre bouton modifier pour bouton récapitulative
		elm.className= "bouton_modifier";	
		var btn = document.getElementById("bouton_toute_commande"); 
		btn.className= "bouton_classique";
		
		document.getElementById("tab_recapitulative").style.display= 'table';
		document.getElementById("titre_tab_recapitulative").style.display= 'block';
		document.getElementById("titre_commande").style.display= 'none';	

		document.getElementById("titre_commande").style.display= 'none';	
		
		var tab_commande = document.getElementsByClassName('tableau_panier');	
		for(var i = 0; i < tab_commande.length; i++)
		 {
			tab_commande[i].style.display = 'none';
		}
		
		var tab_detail = document.getElementsByClassName('detail_tab_commande');	
		for(var i = 0; i < tab_detail.length; i++)
		 {
			tab_detail[i].style.display = 'none';
		}

	}
 } 
 
     var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"90%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
</script>  
	</body>
</html>