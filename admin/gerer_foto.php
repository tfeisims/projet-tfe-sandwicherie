<!-----
réalialiser par: kevin detrrain,
but: projet TFE
année: 2014
utilisé:
Cette page permet  la gestion des photos affiché dans le diaporama.
l'affichage des photo se fait dans les fichiers:
->include/carousel.php
->admin/include/carousel.php

ajout/suppression/modfification
elle fonctionne a laide des page se trouvant dans le dossier admin/fonction_image
------>
<?php
session_start();
include ('verification/verification_acces.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />		
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<link href="css/uploadfile.min.css" rel="stylesheet">		
		<script src="js/jquery.uploadfile.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="../style_ie.css" />
		<![endif]-->
		<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
			include ('include/header.php');
			include ('include/bar_de_menu.php');
		?>
		<div id="conteneur_principal">
			<div id="zone_affichage">
			<!---zone permettant l'ajout de photo--->
				<div>
					<h1 style=" margin-top:0px;">Gestion du diaporama</h1>
					<div  id="startUpload" style="display:inline-block; float:right;border:1px solid black; position: relative; left:20px; top: -5px; padding: 15px 5px 5px 5px; text-align: center; Background-Color: #2f8ab9;	font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: white; margin-right: 40px; margin-top:10px; height:25px; width:90px;  font-weight: bold;cursor:pointer;border-radius: 3px;">Telecharger</div>
					<div id="advancedUpload">Upload</div>
					<div id="status"></div>
					<span id="eventsmessage"></span>
				</div>
				<div>
					<?php require 'include/mysql.inc.php'; 
								$req = $bdd->query("SELECT count(id) as total_image FROM images WHERE id_categorie_image= 1 && active= 1") or die(print_r($bdd->errorInfo()));
								$total= $req->fetch();
								$total_photo_active = $total['total_image'];
					?>
				<!-----zone permettant de visualiser et de gérer les photo ------>
					<h2 id="titre_image" style="display:inline-block;">Vos images<a href="gerer_foto.php"><img src="../img/rafraichir.png" id="rafraichir" onmouseOver="mouse_over_img(this);" onmouseOut="mouse_out_img(this);" style="width:20px;position:relative;top:5px; left:2px;" title="Rafraichir" alt="image_rafrachir"/></a></h2>
					<span style="font-size:18px;position:relative; top:0px;left:400px;">(<?php echo $total_photo_active;?>/18 activées)</span>
					<?php
					//----mesage d'alerte et d'information------
						if(isset($_SESSION['erreur_gestion_image_admin']))
						{
							echo 	"<div class='cadre_alerte' style='margin-bottom:20px;'>
										<img src='../img/attention2.png' alt='img attention' id='img_attention'/>  <span id='texte_alerte'>"
										. $_SESSION['erreur_gestion_image_admin'] ."</span></div>";
										$_SESSION['erreur_gestion_image_admin'] = NULL;
						}
						if(isset($_SESSION['info_gestion_image_admin']))
						{
							echo 	"<div class='cadre_alerte' style='margin-bottom:20px;'>
										<img src='../img/info.png' alt='img info' id='img_info'/>  <span id='texte_alerte'>"
										. $_SESSION['info_gestion_image_admin'] ."</span></div>";
										$_SESSION['info_gestion_image_admin'] = NULL;
						}
						//----------------------------
						$req = $bdd->query("SELECT count(id) as total_image FROM images WHERE id_categorie_image= 1") or die(print_r($bdd->errorInfo()));
						$total= $req->fetch();
						$total_photo = $total['total_image'];
						if($total_photo > 0)
						{
							$req = $bdd->query("SELECT id,url,titre,id_categorie_image,active,lien_image	FROM images WHERE id_categorie_image= 1 ORDER BY id") or die(print_r($bdd->errorInfo()));
					 // liste des photos déjà ajouté	
							while($donnee=$req->fetch())
							{
								echo "<div style='width:100%;margin-bottom:10px;'>
												<div style='display: inline-block; width:155px;'>";								
								if (file_exists('../'.$donnee['url']))
								{
									echo "<img src='../".$donnee['url'] ."' id='img_".$donnee['id']."' style='width:150px;' alt='img_du_carousel'/>";
								}
								else
								{
									echo "<img src='../img/image_inconnu.png' style='width:150px;' alt='image_inconnu'/>";
								}
									echo "</div>
												<div style='display:inline-block;width:480px; height:150px; vertical-align:top;'>
												<div style='width:90px;vertical-align:top;display:inline-block;margin-top:5px;'>
														Commentaire:<br/>
														Lien:<br/>
														Etat:
													</div>
													<div style='display:inline-block;'>
														<form action='fonction_image/edit_photo.php' method='post'>
															<input type='text'  name='commentaire' placeholder='optionnel (maximum 38 caractères)' size=40 value='".$donnee['titre']."'/><br/>
															<input type='text'  name='lien' placeholder='optionnel (ex: www.exemple.be)' value='".$donnee['lien_image']."' size=40/>
															<input type='hidden' name='identifiant' value='".$donnee['id']."'/>
															<input type='submit' value='Modifier' />
														</form>";
														if($donnee['active']==1)
														{
															if (file_exists('../'.$donnee['url']))	//changer (pointer-events: none;cursor: default;) => par une variation de class ex: .active , .desactive
															{													//permet de désactivé le lien Activé dans le cas qu'il y aurait une erreur "image non trouvé"
																echo "<span style='color:green;text-decoration: underline;cursor:pointer;'>Activer</span> / <a href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=desactive' style='color:black;'>Désactiver</a> / <a style='color:black;' href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=supprime'>Supprimer</a>";
															}
															else						//idem
															{	
																echo "<span style='color:gray;'>Activer</span> / <a href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=desactive' style='color:black;'>Désactiver</a> / <a style='color:black;' href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=supprime'>Supprimer</a>";
															}
														}
														else
														{
															if (file_exists('../'.$donnee['url']))	//idem
															{	
																echo "<a href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=active' style='color:black'>Activer</a> / <span style='color:red;text-decoration: underline;cursor:pointer;'>Désactiver</span> / <a style='color:black;' href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=supprime'>Supprimer</a>";
															}
															else					//idem
															{
																echo "<span style='color:gray;'>Activer</span> /<span style='color:red;text-decoration: underline;'>Désactiver</span> / <a style='color:black;' href='fonction_image/modification_etat.php?id=".$donnee['id']."&&etat=supprime'>Supprimer</a>";
															}
														}
											echo	"</div>
												</div>
											</div>";
							}
						}
						else
						{
							echo '<p style="margin: 5px 5px 0px 5px;">Vous ne disposez pas encode de photo,</p>
								<p style="margin: 5px 5px 0px 10px;">Pour ajouter une photo:</p>
								<p style="margin: 5px 5px 0px 15px;">1. Cliquez sur "Upload" ou faites un glisser-déposer avec les photos que vous désirez ajouter.
							<br/><span style="font-style:italic;">PS: Vous pouvez ajouter plusieurs photos en même temps.</span>
							<br/>2. Cliquez sur "Télécharger" après avoir ajouté les photos que vous désirez.
							<br/>3. Rafraîchissez la page à l\'aide du bouton "Rafraîchir" <img src="../img/rafraichir.png" style="border:1px solid black; width:15px;position:relative;top:1px; left:0px;"/> .
							<br/>4. Vos images sont maintenant disponibles:</p>
							<p style="margin: 2px 5px 0px 20px;">-> Vous les rendrez visibles dans le diaporama en cliquant sur "Activer".
							<br/>-> Vous les rendrez invisibles dans le diaporama en cliquant sur "Désactiver".
							<br/>-> Vous pouvez ajouter des commentaires et des liens sur vos images à l\'aide des deux champs et du bouton "Modifier".
							<br/>-> Vous pouvez supprimer les photos que avez ajoutées en cliquant sur "Supprimer".</p>
							<p style="margin: 5px 5px 0px 5px;"><span style="text-decoration: underline;">Remarque</span>: Le carrousel  ne permet pas d\'avoir plus de 18 photos activées simultanément.</p>
							';
						}
					?>
				</div>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
	<script>
$(document).ready(function()
{
var uploadObj = $("#advancedUpload").uploadFile({
		url:"fonction_image/upload.php",
		multiple:true,
		autoSubmit:false,
	    allowedTypes:"jpg,png",
		fileName:"myfile",
		showStatusAfterSuccess:false,
		dragDropStr: "<span><b>Faites un glisser-déposer avec vos photos</b></span>",
		abortStr:"Abandonner",
		cancelStr:"Annuler",
		doneStr:"fait",
		multiDragErrorStr: "Plusieurs Drag & Drop de fichiers ne sont pas autorisés.",
		extErrorStr:"n'est pas autorisé. Extensions autorisées:",
		sizeErrorStr:"n'est pas autorisé. Admis taille max:",
		uploadErrorStr:"Upload n'est pas autorisé",
		onSuccess:function(files,data,xhr)
		{
			$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Envoi reussi pour: "+JSON.stringify(data));
			
		},
		onError: function(files,status,errMsg)
		{
			$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Echec de l'envoie pour: "+JSON.stringify(files));
		}
		});
	$("#startUpload").click(function(){
		uploadObj.startUpload();
	});
});
	</script>
	<script type="text/javascript" src="js/monJQ.js"></script>	
	</body>
</html >
