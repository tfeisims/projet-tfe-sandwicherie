<?php
// Include the main TCPDF library (search for installation path).
require_once('tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		$image_file = K_PATH_IMAGES.'logo.png';
		$this->Image($image_file, 2, 4, 45, '', 'PNG', '', 'T', false, 300, '', false, false, 1, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 18);
		// Title
		$this->Cell(0, 20, 'Liste des commandes', 1, 1, 'C', false, false, 1, false, 'T', 'C');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$image_file = K_PATH_IMAGES.'HEH.jpeg';
		$this->Image($image_file,160, 278, 18, '', 'JPEG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$image_file2 = K_PATH_IMAGES.'campus_tech.png';
		$this->Image($image_file2, 179, 279, 17, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
	}
	// Tableau coloré
	function ColoredTable($header,$tab)
	{
		// Couleurs, épaisseur du trait et police grasse
		$this->SetFillColor(120,120,120);
		$this->SetTextColor(255);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		// En-tête
		$w = array(110, 25, 20, 30);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
		$this->Ln();
		// Restauration des couleurs et de la police
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		// Données
		$fill = false;
		$total = 0;
		foreach($tab as $row)
		{
			$this->Cell($w[0],4,$row[0],'LR',0,'L',$fill);
			$this->Cell($w[1],4,$row[1],'LR',0,'C',$fill);
			$this->Cell($w[2],4,number_format($row[2],0,',',' '),'LR',0,'C',$fill);
			$total +=($row[1]*$row[2]);
			$total_cellule = $row[1]*$row[2];
			$this->Cell($w[3],4,number_format($total_cellule,2,',',' '),'LR',0,'C',$fill);
			$this->Ln();
			$fill = !$fill;
		}
		// Trait de terminaison
		$this->Cell(array_sum($w),0,'','T');
		$this->Ln(0);
		$this->SetFillColor(235,235,235);
		$this->Cell(155,8,'Total à payer: ',1,0,'R','LR',0,$fill);
		$this->SetFillColor(220,220,220);
		$this->Cell(30,8,number_format($total,2,',',' '),1,0,'C','LR',0,$fill);

	}
}
//--------------------------------------------base de donnée-------------------------------------------------
//création d'un objet PDO pour permettre la connexion avec la base de donnée
include_once('../include/mysql.inc.php');
$req = $bdd->query("SELECT  MAX(date_cree) as max_date  FROM commandes WHERE confirmation=1") 	or die(print_r($bdd->errorInfo()));
$donnee= $req->fetch();
$date_dernier_comm = date("d/m/Y", strtotime($donnee['max_date']));
//variables pour récupérer les commandes réalisé
$heure_du_jour_ouverture=date( 'Y-d-m 7:00:00', strtotime( $date_dernier_comm ) );
$heure_du_jour_fermeture=date( 'Y-d-m 23:00:00', strtotime( $date_dernier_comm ) );
$date_com = $date_dernier_comm;

		if(isset($_GET['d_commande']))
		{
			$date_commande = explode("/", $_GET['d_commande'] );
			$date_affiche_commande = ''.$date_commande[0].'-'.$date_commande[1].'-'.$date_commande[2].'';
			$heure_du_jour_ouverture=date( 'Y-m-d 7:00:00', strtotime( $date_affiche_commande ) );
			$heure_du_jour_fermeture=date( 'Y-m-d 23:00:00', strtotime( $date_affiche_commande ) );
			$date_com = $date_affiche_commande;
		}
/*--------------------------requête pour obtenir les commandes pour chaque utilisateur---------------------*/
$req = $bdd->query("SELECT c.id_client as id_client, c.quantite as quantite_produit, c.date_cree as date_cree,DATE(c.date_cree) as date_jour, c.confirmation as confirmation, c.id_produit, c.commentaire as commentaire,p.id, p.nom as produit, p.prix as prix_produit	
							FROM commandes c
							INNER JOIN produits p
							ON p.id = c.id_produit
							WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1'
							ORDER BY date_jour,id_client")
or die(print_r($bdd->errorInfo()));

/*--------------------------requête pour obtenir un récapitulative des commandes réaliser---------------------*/
$commande_req = $bdd->query("SELECT  c.id_produit,SUM(c.quantite) AS quantite_total,c.date_cree,c.confirmation,p.id, p.nom, p.prix
		FROM commandes c
		INNER JOIN produits p
		ON p.id = c.id_produit
		WHERE date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1'
		GROUP BY c.id_produit
		ORDER BY p.nom
		")
		or die(print_r($bdd->errorInfo()));
//----------------------------------------------------base de donnée-------------------------------------------



// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// set header and footer fonts
$pdf->Header($date_com);
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// add a page
$pdf->AddPage();
// Titres des colonnes


//----------------------------------------------------------------------------------------------------------création des tableaux pour chaque client-------------------------------
$header = array('Nom du produit', 'Prix unitaire', 'Quantité', 'Total');
// Chargement des données
$num_commande = 1;
$i = 0;
$utilisateur_temp = null;
$commentaire_temp = null;
$date_temp = null;
$tab = array(array());
$pdf->ln(4);
$pdf->Cell(0, 10, 'Liste des commandes triées par client', 0, 1, 'L');	// TITRE 1
$pdf->ln(-10);
while($donnee=$req->fetch())
{
	// set font
	$pdf->SetFont('times', 'BI', 12);
	//on charge d'abord  un tableau donc l'utilisateur est commun à une date précise
	if($utilisateur_temp == null || $utilisateur_temp == $donnee['id_client'])
	{
		$tab[$i][0] = $donnee['produit'];
		$tab[$i][1] = $donnee['prix_produit'];
		$tab[$i][2] = $donnee['quantite_produit'];
		$i++;
	}
	else	//lorsque l'utilisateur n'est plus commun on envoye le resultat au tableau pour l'afficher
	{
				// Restauration des couleurs et de la police
		$pdf->SetFillColor(235,235,235);
		$pdf->Ln(12);
		$pdf->Cell(120,4,'Commande n°'.$num_commande.'',1,0,'LR',true);
		$pdf->SetFillColor(250,250,250);
		$pdf->SetFont('times', '', 12);
		$pdf->Cell(65,4,'Date/Heure: '.$date_temp.'',1,0,'LR',true);
		$pdf->SetFillColor(235,235,235);
		$pdf->SetFont('times', 'BI', 12);
		$pdf->ln();
		$pdf->Cell(185,4,'Réalisé par: '.$utilisateur_temp,1,1,'LR',true);
		$pdf->MultiCell(185,5,'Commentaire: '.$commentaire_temp.'.',1,'LR',true,1,'','',true,0,false,true,0,'T',true);
		//$pdf->MultiCell(185,5,'Commentaire: '.$commentaire_temp.'.',1,1,'LR',true);
		//$pdf->Cell(185,5,'Commentaire: '.$commentaire_temp.'.',1,1,'LR',true);
		$pdf->ColoredTable($header,$tab);
		$num_commande++;
		//------------------------------création de la table dans le cas que l'utilisateur est différent selement la premiere ligne de tableau
		$i=0;
		$tab = array(array());	// je vide le tableau pour m'assuré qu'il n'y a plus rien dedans
		$tab[$i][0] = $donnee['produit'];
		$tab[$i][1] = $donnee['prix_produit'];
		$tab[$i][2] = $donnee['quantite_produit'];
		$i++;

}	
	$utilisateur_temp = $donnee['id_client'];	
	$commentaire_temp = wordwrap($donnee['commentaire'], 80, "\n", true);
	$date_temp =  date("d/m/Y H:m:s", strtotime($donnee['date_cree']));

}
//------création du dernier tableau
		$pdf->SetFillColor(235,235,235);
		$pdf->Ln(12);
		$pdf->Cell(120,4,'Commande n°'.$num_commande.'',1,0,'LR',true);
		$pdf->SetFillColor(250,250,250);
		$pdf->SetFont('times', '', 12);
		$pdf->Cell(65,4,'Date/Heure: '.$date_temp.'',1,0,'LR',true);
		$pdf->SetFillColor(235,235,235);
		$pdf->SetFont('times', 'BI', 12);
		$pdf->ln();
		$pdf->Cell(185,4,'Réalisé par: '.$utilisateur_temp,1,1,'LR',true);
	//	$pdf->Cell(185,5,'Commentaire: '.$commentaire_temp.'.',1,1,'LR',true);
		$pdf->MultiCell(185,5,'Commentaire: '.$commentaire_temp.'.',1,'LR',true,1,'','',true,0,false,true,0,'T',true);
	$pdf->ColoredTable($header,$tab);
	$num_commande++;
//------------------------------------------------
		$i=0;
		$tab = array(array());	// je vide le tableau pour m'assuré qu'il n'y a plus rien dedans
		$tab[$i][0] = $donnee['produit'];
		$tab[$i][1] = $donnee['prix_produit'];
		$tab[$i][2] = $donnee['quantite_produit'];
		
//----------création du tableau récapitulative----------------------------------------------
//$pdf->ln(10);
$pdf->AddPage();
// Set font
$pdf->SetFont('helvetica', 'B', 18);
$pdf->Cell(0, 10, 'Tableau récapitulatif', 0, 1, 'L');	//TITRE 2
// set font
$pdf->SetFont('times', 'BI', 12);
$header = array('Nom du produit', 'Prix unitaire', 'Quantité', 'Somme total');
while($commande=$commande_req->fetch())
{
		$tab[$i][0] = $commande['nom'];
		$tab[$i][1] = $commande['prix'];
		$tab[$i][2] = $commande['quantite_total'];
		$i++;
}
$pdf->ColoredTable($header,$tab);

//Close and output PDF document
$date_sauvegarde = str_replace("/","-",$_GET['d_commande']);
$pdf->Output('Commande du '.$date_sauvegarde.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>