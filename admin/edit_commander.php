﻿<?php
session_start();
include ('verification/verification_acces.php');	
	require 'include/mysql.inc.php';
	//on recupère le nombre de catégorie total pour évité que l'on introduise un numéro de catégorie qui n'existe pas
	$req = $bdd->query('SELECT COUNT(id) AS nombre_cat FROM categorie');
	$donnee = $req->fetch();
	$req->closeCursor();
	
	// on verifie que le contenu existe
	if(isset($_POST['nom_produit']) && is_string($_POST['nom_produit']) && isset($_POST['id_produit']) && ctype_digit($_POST['id_produit']) && isset($_POST['disponible']) && ctype_digit($_POST['disponible']) && isset($_POST['prix_produit']) && is_numeric($_POST['prix_produit']) && ctype_digit($_POST['categorie']) && isset($_POST['categorie']))
	{
	$produit = htmlspecialchars($_POST['nom_produit']);	//convertion format UTF-8 en ISO8859-1 car problème 
	$id= htmlspecialchars($_POST['id_produit']);
	$disponible= htmlspecialchars($_POST['disponible']);
	$prix = htmlspecialchars($_POST['prix_produit']);
	$cat = htmlspecialchars($_POST['categorie']);
	
		//on vérifie que l'on reçoi bien un booléan
		if($disponible == "1" || $disponible == "0")
		{	
			//on verifie que l'on entre un nombre correcte dans le champs "prix" et inférieur à une certaine somme
			if(strlen($prix) <=5 && $prix <= 20 && $prix >= 0)
			{	//on vérifie que le numéro de catégorie est correcte
				if($cat > 0 && $cat <= $donnee['nombre_cat'])
				{
					$req = $bdd->prepare('UPDATE produits SET nom = :nom_produit,prix= :prix_produit,id_categorie= :categorie_produit, disponible=:disponible_produit WHERE id = :id_produit');
					$req->execute(array(
					'nom_produit' => $produit,
					'prix_produit' => $prix,
					'categorie_produit' => $cat,
					'disponible_produit' => $disponible,
					'id_produit' => $id
					));
					$_SESSION['info_commander_admin'] = " Le produit a été modifié.";
					header('location: commander_admin.php');	
				}
				else
				{
					$_SESSION['erreur_commander_admin'] ="Vous avez entrez un numéro de catégorie incorrect.(entre 1 et ".$donnee['nombre_cat'].")";
					header('location: commander_admin.php');
				}
			}
			else
			{
				$_SESSION['erreur_commander_admin'] ="Vous avez entré une valeur erronée dans le champs \"prix\".";
				header('location: commander_admin.php');
			}
		}
		else
		{
			$_SESSION['erreur_commander_admin'] ="Vous avez entré une valeur erronée dans le champ 'disponible'.";
			header('location: commander_admin.php');
		}
	}
	else
	{	
		$_SESSION['erreur_commander_admin'] ="L'un de vos champs est vide ou a reçu une valeur erronée.";
		header('location: commander_admin.php');
	}
?>