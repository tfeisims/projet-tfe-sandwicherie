<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="../coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="../style.css" />
		<link rel="icon" type="image/png" href="../img/favicon.ico" />
		<script type="text/javascript" src="../jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="../style_ie.css" />
        <![endif]-->
	<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
		?>
		<div id="conteneur_principal">
			<!-----zone central contenant les élément important---------------------->
			<div id="zone_affichage">
			<?php
				if(!empty($_SESSION['erreur']))
				{
						echo "<div id='erreur_connexion'>".$_SESSION['erreur']."</div>";
						$_SESSION['erreur'] = NULL;
				}
			?>
					<h1>Tableau de bord</h1>
					<p style='text-align:justify;'>Bienvenue dans la partie adminstrative du site, celle-ci vous permet d'organiser la liste des produits que vous souhaitez mettre en vente, afficher les  commandes, avoir un aperçu des statistiques des ventes et  gérer une partie de l'apparence du site de la sandwicherie du Campus Technique de Mons.</p>
					<p style='text-align:justify;'>Pour tous renseignements supplémentaires conçernant l'heure d'ouverture et les procédures à suivre pour obtenir la commande, veuillez accéder à la partie <a href="info_admin.php">Infos</a>.</p>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
			</div>
			<?php
				include ('include/footer.php');		
			?>
		<script type="text/javascript" src="js/monJQ.js"></script>		
	</body>
</html>