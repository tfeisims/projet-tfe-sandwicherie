<?php
session_start();

if(isset($_SESSION['active']))	//on vérifie que la personne est bien connecté
{
	if(isset($_SESSION['panier']))	//on vérifie qu'elle a un panier
	{
	$nbrElements = count($_SESSION['panier']['id_produit']);
		if($nbrElements>0 && isset($_POST["exigences"]))		//on vérifie que le panier posséde au moins 1 éléments.
		{
			$exigence = mysql_real_escape_string(htmlspecialchars($_POST["exigences"]));
			if(strlen($exigence) <255)	// on vérifie que la chaine de caractères envoyé par le client ne dépasse pas 255 caractères.
			{
				if($_POST["total_a_payer"] <= 20) // on vérifie que le montant total de la commande ne dépasse pas 20€
				{
					setlocale(LC_TIME, 'french');
					date_default_timezone_set('Europe/Paris');
					$heure = date("H:i:s");
					$heure_ouverture = date("07:00:00");				//ouverture du site
					$heure_fermeture_panier = date("11:00:00");
					if ($heure_ouverture < $heure && $heure_fermeture_panier > $heure)	// on vérifie que le client confirme bien sa commande entre 7h et 11h
					{
						// On se connecte à  MySQL
						require '../include/mysql.inc.php';
						
						for($i=0;$i<$nbrElements;$i++)
						{													//pas oublier de rajouter le champs "exigences" !!!!!!!!!!
						$req = $bdd->prepare('INSERT INTO commandes (id_client, id_produit,quantite, date_cree,commentaire,confirmation) VALUES(:id_client,:id_produit, :quantite, NOW(), :commentaire, :confirmation)');
								$req->execute(array(
									'id_client' => $_SESSION['id'],
									'id_produit' => $_SESSION['panier']['id_produit'][$i],
									'quantite' => $_SESSION['panier']['quantite'][$i],
									'commentaire' => $exigence,
									'confirmation' => "1"
									
									)) or die(print_r($bdd->errorInfo()));	
						}
						$_SESSION['info_panier'] = "Votre commande a bien été effectuée.";
						header('Location: ../panier.php');
					}
					else
					{
						$_SESSION['panier'] = array();
						$_SESSION['panier']['id_produit'] = array();
						$_SESSION['panier']['quantite'] = array();
						$_SESSION['info_panier'] = "Vous n'avez pas confirmée votre commande entre 7h et 11h.";
						header('Location: ../panier.php');
					}
				}
				else
				{
				$_SESSION['erreur_edit_panier'] = 'Le montant total de la commande a été limité à 20€.';
				header('Location: ../panier.php');
				}
			}
			else
			{
				$_SESSION['erreur_edit_panier'] = 'vous avez dépassé la limite de caractères que nous vous avons imposé.';
				header('Location: ../panier.php');
			}
		}
		else
		{
			$_SESSION['erreur_edit_panier'] = 'Commande non terminer: votre panier est vide.';
			header('Location: ../panier.php');
		}
	}
	else
	{
			$_SESSION['erreur_edit_panier'] = 'Commande non terminer: vous n\'avez pas encore effectuer d\'achat.';
			header('Location: ../panier.php');
	}
}
else
{
	$_SESSION['erreur_edit_panier'] = 'Commande non terminer: Vous ne vous êtes pas encore connecté.';
	header('Location: ../panier.php');
}
?>