<?php
	session_start();

	setlocale(LC_TIME, 'french');
	date_default_timezone_set('Europe/Paris');
	$heure = date("H:i:s");
	$date_actuel = date("Y-m-d");
	$jour_actuel = strftime('%A', strtotime($date_actuel));
	$heure_ouverture = date("07:00:00");				//ouverture du site
	$heure_fermeture_panier = date("11:00:00");
	$heure_fermeture = date("11:00:00");
	if($jour_actuel != "samedi" && $jour_actuel != "dimanche")
	{
		if(($heure_ouverture < $heure && $heure_fermeture > $heure))
		{
			// on verifie que le contenu existe et qu'il s'agit bien d'un nombre	
			if(isset($_POST['id_produit']) && ctype_digit($_POST['id_produit']) && isset($_POST['quantite']) && ctype_digit($_POST['quantite']))
			{
				$id_produit= mysql_real_escape_string(htmlspecialchars($_POST['id_produit']));
				$quantite =mysql_real_escape_string(htmlspecialchars($_POST['quantite']));
				$_SESSION['erreur_panier'] = NULL;
				$nbArticles = 0;	//initiation du nombre de commande déja effectué dans la bse de donnée
				if(isset($_SESSION['id'])) // si l'utilisateur est connecté, on vérifie s'il a déja valider ou non sa commande
				{
					include_once '../include/mysql.inc.php';
					$heure_du_jour_ouverture = date("Y-m-d 7:00:00");		// récupération dans la base( période où l'on prend compte de la commande réalisé dans la journée)
					$heure_du_jour_fermeture = date("Y-m-d 20:00:00");		// idem		
				
					$req = $bdd->query("SELECT id_client, SUM(quantite) as quantite_total, quantite,date_cree,confirmation	FROM commandes WHERE id_client = '".$_SESSION['id']."' && date_cree > '".	$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1' GROUP BY id_client") or die(print_r($bdd->errorInfo()));
					$quantite_total_produit = $req->fetch();
					if($quantite_total_produit['quantite_total'] != null)	// on modifie la valeur se trouvant dans la base de donnée que s'il n'est pas null donc = à 0
					$nbArticles=$quantite_total_produit['quantite_total'];
				}
				
				if($nbArticles == 0)	// s'il n'a pas encore valider une commande
				{
					include_once '../include/mysql.inc.php';
					$dispo_req = $bdd->query("SELECT id,disponible FROM produits WHERE id=".$id_produit."") or die(print_r($bdd->errorInfo()));
					$disponible = $dispo_req->fetch();
					if($disponible['disponible'] == 1) // s'il essaye de commander un produit indisponible
					{
						//Création du panier
						if(!isset($_SESSION['panier']))
						{
							$_SESSION['panier'] = array();
							$_SESSION['panier']['id_produit'] = array();
							$_SESSION['panier']['quantite'] = array();
						}

						//on vérifie que la quantié est suppérieur à zero
						if($quantite>0)
						{
							if($quantite<=5)
							{
								//on vérifie que le produit n'existe pas déja dans les produits déja commander
								$positionProduit = array_search($id_produit, $_SESSION['panier']['id_produit']);

								if($positionProduit !== FALSE)	// si le produit existe déja
								{
									//on vérifie que lors que l'on ajoute d'autres articles semblables que nous ne dépassons pas la limite autorisé.
									if(($_SESSION['panier']['quantite'][$positionProduit] + $quantite) <= 5)
									{
										$_SESSION['panier']['quantite'][$positionProduit] += $quantite;
										$_SESSION['info_panier'] = "La quantité de ce produit a été augmenté.";
										header('Location: ../commander.php');
										exit;
									}
									else
									{
										$_SESSION['erreur_panier'] = 'Vous avez dépassé la limite de 5 articles.';
										header('Location: ../commander.php');
										exit;
									}
								}
								else			// s'il n'existe pas
								{
									array_push($_SESSION['panier']['id_produit'], $id_produit);
									array_push($_SESSION['panier']['quantite'], $quantite);
									$_SESSION['info_panier'] = "Le produit a été ajouté à votre panier.";
									header('Location: ../commander.php');
									exit;
								}		
							}
							else
							{
								$_SESSION['erreur_panier'] = 'Vous ne pouvez pas commander plus de 5 articles.';
								header('Location: ../commander.php');
								exit;
							}
						}
						else
						{
							$_SESSION['erreur_panier'] = 'Vous avez entré une valeur nulle ou négative dans le champs quantité.';
							header('Location: ../commander.php');
							exit;
						}
					}
					else
					{
							$_SESSION['erreur_panier'] = 'Vous n\'avez pas le droit d\'ajouter ce produit.';
							header('Location: ../commander.php');
							exit;
					}
				}
				else
				{
						$_SESSION['erreur_panier'] = 'Vous avez déjà confirmé votre commande.';
						header('Location: ../commander.php');
				}
			}
			else
			{
				$_SESSION['erreur_panier'] = 'Vous avez entré une valeur incorrecte dans le champs quantité.';
				header('Location: ../commander.php');
				exit;
			}
		}
		else
		{
			$_SESSION['erreur_panier'] = "Le produit n'a pas été ajouté, la plateforme n'est active que de 7h à 11h.";
			header('Location: ../commander.php');
			exit;
		}
	}
	else
	{
		$_SESSION['erreur_panier'] = "Le produit n'a pas été ajouté, La plateforme n'est pas activée le weekend.";
		header('Location: ../commander.php');
		exit;
	}
?>