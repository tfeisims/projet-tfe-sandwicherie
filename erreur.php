<?php
session_start();
require_once 'class/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="language" content="FR" />	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="style.css" />
		<link rel="icon" type="image/png" href="img/favicon.ico" />
		<script type="text/javascript" src="jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if gt IE 7]>
			<link rel="stylesheet" href="style_ie.css" />
        <![endif]-->
	<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
			<?php
		$check2 = $detect->isMobile();
		//s'il s'agit d'un mobile on applique le style mobile
		if($check2)
			echo '<link rel="stylesheet" href="style_mobile.css" />';
	?>
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
		?>	
		<div id="conteneur_principal">
			<!-----zone central contenant les élément important---------------------->
			<div id="zone_affichage">
					<h1>Accès interdit</h1>
					<p>Vous n'avez pas le droit de vous trouver dans cette partie du site.
					<br/><a href="index.php">Retourner à la page d'accueil</a></p>
					

			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script type="text/javascript" src="js/monJQ.js"></script>	
	</body>
</html>