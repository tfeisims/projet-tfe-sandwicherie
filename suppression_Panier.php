<?php
	session_start();

		setlocale(LC_TIME, 'french');
	date_default_timezone_set('Europe/Paris');
	$heure = date("H:i:s");
	$date_actuel = date("Y-m-d");
	$jour_actuel = strftime('%A', strtotime($date_actuel));
	$heure_ouverture = date("07:00:00");				//ouverture du site
	$heure_fermeture_panier = date("11:00:00");
	$heure_fermeture = date("11:00:00");
	if($jour_actuel != "samedi" && $jour_actuel != "dimanche")
	{
		if(($heure_ouverture < $heure && $heure_fermeture > $heure))
		{		
			if(isset($_GET['id_produit']) &&  ctype_digit($_GET['id_produit']))
			{
				$id_produit = mysql_real_escape_string($_GET['id_produit']);

				$temp['panier'] = array();
				$temp['panier']['id_produit'] = array();
				$temp['panier']['quantite'] = array();

				$nbrArticles = count($_SESSION['panier']['id_produit']);

				for($i=0; $i<$nbrArticles; $i++)
				{
					if($_SESSION['panier']['id_produit'][$i] != $id_produit)
					{
						array_push($temp['panier']['id_produit'], $_SESSION['panier']['id_produit'][$i]);
						array_push($temp['panier']['quantite'], $_SESSION['panier']['quantite'][$i]);
					}
				}

				$_SESSION['panier'] = $temp['panier'];
				$_SESSION['info_panier'] = "Le produit a été supprimé.";
			}

			header('Location: panier.php');
			exit;
		}
		else
		{
			//on vide le panier
			$_SESSION['panier'] = array();
			$_SESSION['panier']['id_produit'] = array();
			$_SESSION['panier']['quantite'] = array();
			$_SESSION['erreur_edit_panier'] = "Opération impossible, la plateforme n'est active que de 7h à 11h.";
			header('Location: panier.php');
			exit;
		}
	}
	else
	{	//on vide le panier
		$_SESSION['panier'] = array();
		$_SESSION['panier']['id_produit'] = array();
		$_SESSION['panier']['quantite'] = array();
		$_SESSION['erreur_edit_panier'] = "Opération impossible, La plateforme n'est pas activée le weekend.";
		header('Location: panier.php');
		exit;
	}					
			
?>