<!-----
réalialiser par: kevin detrrain,
but: projet TFE
année: 2014
utilité:
cette page affiche les news
les annonces sont générer dans la partie admin
------>
<div id="zone_news">
	<div id="tableau_news">
		<img src="img/tableau.jpg" alt="image_d'un_tableau" style="width:300px; height:400px;"/>
		<div id="texte_news">
				<h3 style="text-align: center;margin:0px;">Annonces et Nouveautés</h3>
			<?php 
				require_once 'include/mysql.inc.php';
				//permet d'obtenir la date d'y a 7 jours
				//$date = strftime("%y-%m-%d %H-%M-%S", mktime(0, 0, 0, date('m'), date('d')-7));
		try {				
				$req_annonce = $bdd->query("SELECT * FROM news WHERE active= 1 ORDER BY id DESC LIMIT 0,4") or die(print_r($bdd->errorInfo()));
				while($annonce=$req_annonce->fetch())
				{
										//on vérifie si la date de modification est plus récente ou non que la date de création
					if($annonce['date_modif'] > $annonce['date_cree'])
					$date_affiche = $annonce['date_modif'];
					else
					$date_affiche = $annonce['date_cree'];
					//on convertie la date la plus récente
					$date_annonce = date("d/m/Y H:m:s", strtotime($date_affiche));
					echo '<p style="width:270px;word-wrap: break-word;text-align:left;margin-top:10px;margin-left:6px;">'.$annonce["message"].'<br/><span style="font-size:12px; float:right;color:gray;">'.$date_annonce.'</span></p>
					<div style="border-top:1px solid gray;width:120px;margin:auto;"></div>';
				}	
		}
 catch (PDOException $e){
     //echo 'Erreur de connexion !!! :' . $e->getMessage() . '<br/>';
	echo 'une erreur s\'est produite, veuillez contacter le service informatique.<br/>';
	 exit();
 }				
				?>
		</div>
	</div>
		<!-----zone gauche de la page avec cadre panorama------>
		<?php
			include ('include/carousel.php');
		?>
</div>