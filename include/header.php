		<div id="header">
	        <SCRIPT LANGUAGE="JavaScript">
				// Datetime courant, au chargement de la page
				<?php
				setlocale(LC_TIME, 'french');
				date_default_timezone_set('Europe/Paris');				
				?>
				heure = <?php echo date("H");?>;
				min = <?php echo date("i");?>;
				sec = <?php echo date("s");?>;
				jour = <?php echo date("d");?>;
				mois = <?php echo date("m");?>;
				annee = <?php echo date("Y");?>;
				jour_semaine = <?php echo date("N");?>;
				mois_ecr = new Array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
				jours = new Array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche');
				// Construction d'un objet Date
				var date = new Date(annee, mois, jour, heure, min, sec);
				 
				function Horloge()
				{
				 
					// Seconde suivante
					date.setSeconds(sec+1);
				 
					// Mise à jour des variables globales
					heure = date.getHours();
					min = date.getMinutes();
					sec = date.getSeconds();
					jour = date.getDate();
					mois = date.getMonth()+1;
					annee = date.getFullYear();
				 
					// Affichage
					if(date.getSeconds() > 10 && date.getMinutes() > 10 )
					{
						affichageHeure =  jours[jour_semaine-1]+' '+date.getDate()+' '+mois_ecr[date.getMonth()-1]+' '+date.getFullYear()+",  "+date.getHours()+':'+ date.getMinutes()+':'+date.getSeconds();
					}
					else if (date.getSeconds() < 10 && date.getMinutes() > 10 )
					{
						affichageHeure =  jours[jour_semaine-1]+' '+date.getDate()+' '+mois_ecr[date.getMonth()-1]+' '+date.getFullYear()+",  "+date.getHours()+':'+ date.getMinutes()+':0'+date.getSeconds();
					}
					else if (date.getSeconds() > 10 && date.getMinutes() < 10 )
					{
						affichageHeure =  jours[jour_semaine-1]+' '+date.getDate()+' '+mois_ecr[date.getMonth()-1]+' '+date.getFullYear()+",  "+date.getHours()+':0'+ date.getMinutes()+':'+date.getSeconds();
					}
										else if (date.getSeconds() < 10 && date.getMinutes() < 10 )
					{
						affichageHeure =  jours[jour_semaine-1]+' '+date.getDate()+' '+mois_ecr[date.getMonth()-1]+' '+date.getFullYear()+",  "+date.getHours()+':0'+ date.getMinutes()+':0'+date.getSeconds();
					}

					//date.toLocaleString();
				 
					if (document.getElementById)
					{
						document.getElementById("date_heure").innerHTML=affichageHeure;
					}
				}
				 
				// Recommencer chaque seconde
				window.onload = function()
				{
					setInterval(Horloge, 1000);
				}
				</SCRIPT>
			<div id="info_header">
				<span style="float:left;white-space:nowrap;" id="date_heure"></span>
				<span style="float: right;">
					<?php
						if(isset($_SESSION['active']) && isset($_SESSION['prenom']) && isset($_SESSION['nom']))
						{	echo  "Bonjour ". $_SESSION['nom'] . " " . $_SESSION['prenom'] . ",";
							if(isset($_SESSION['compte']) && $_SESSION['compte']  == "administrateur")
							echo "<br/><a href='admin/accueil_admin.php' class='lien_detail'>accéder à la page d'administration</a>";
						}
						?>
				</span>
			</div>
				<div>
					<a href="index.php"><img src="img/logo.png" id="logo_principal"  style="display:block;" alt="logo Cafet'Isa" BORDER="0"/></a>
				</div>
		</div>