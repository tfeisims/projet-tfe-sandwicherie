<?php

define('USER', 'client_TFE');
define('MDP', 'test');
define('DNS','mysql:host=localhost;dbname=bd_sandwicherie');

try {
    $bdd = new PDO(DNS, USER, MDP, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 catch (PDOException $e){
    // echo 'Erreur de connexion !!! :' . $e->getMessage() . '<br/>';
	echo 'une erreur s\'est produite, veuillez contacter le service informatique.<br/>';
     exit();
 }
?>
