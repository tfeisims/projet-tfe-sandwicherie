<?php
	session_start();
	
		setlocale(LC_TIME, 'french');
	date_default_timezone_set('Europe/Paris');
	$heure = date("H:i:s");
	$date_actuel = date("Y-m-d");
	$jour_actuel = strftime('%A', strtotime($date_actuel));
	$heure_ouverture = date("07:00:00");				//ouverture du site
	$heure_fermeture_panier = date("11:00:00");
	$heure_fermeture = date("11:00:00");
	if($jour_actuel != "samedi" && $jour_actuel != "dimanche")
	{
		if(($heure_ouverture < $heure && $heure_fermeture > $heure))
		{
			// on verifie que le contenu existe et qu'il s'agit bien d'un nombre
			if(isset($_POST['id_produit']) && ctype_digit($_POST['id_produit']) && isset($_POST['quantite']) && ctype_digit($_POST['quantite']))
			{
				$id_produitProduit =  mysql_real_escape_string(htmlspecialchars($_POST['id_produit']));
				$quantite =  mysql_real_escape_string(htmlspecialchars($_POST['quantite']));
				$_SESSION['erreur_edit_panier'] = NULL;

				if($quantite>0)	//on vérifie si la quantité est supérieur à 0
				{
					if($quantite<=5)	//on vérifie si la quantité est inférieur ou égal à 5
					{
						$positionProduit = array_search($id_produitProduit, $_SESSION['panier']['id_produit']);

						if($positionProduit !== FALSE)
						{
							$_SESSION['panier']['quantite'][$positionProduit] = $quantite;
							$_SESSION['info_panier'] = "La quantité de ce produit a bien été modifié.";
						}
						else
						{
							$_SESSION['erreur_edit_panier'] = 'Le produit n\'existe pas. Bizarre !';
						}

						header('Location: panier.php');
						exit;
					}
					else
					{
						$_SESSION['erreur_edit_panier'] = 'Vous ne pouvez pas commander plus de 5 articles.';
						header('Location: panier.php');
					}
				}
				else
				{
						$_SESSION['erreur_edit_panier'] = 'Vous avez entrez une valeur incorrecte.';
						header('Location: panier.php');
						exit;
				}
			}
			else
			{
				$_SESSION['erreur_edit_panier'] = 'Vous avez entrez autre chose qu\'un chiffre dans le champs quantite.';
				header('Location: panier.php');
				exit;
			}
		}
		else
		{
				//on vide le panier
			$_SESSION['panier'] = array();
			$_SESSION['panier']['id_produit'] = array();
			$_SESSION['panier']['quantite'] = array();
			$_SESSION['erreur_edit_panier'] = "Opération impossible, la plateforme n'est active que de 7h à 11h.";
			header('Location: panier.php');
			exit;
		}
	}
	else
	{
		//on vide le panier
		$_SESSION['panier'] = array();
		$_SESSION['panier']['id_produit'] = array();
		$_SESSION['panier']['quantite'] = array();
		$_SESSION['erreur_edit_panier'] = "Opération impossible, La plateforme n'est pas activée le weekend.";
		header('Location: panier.php');
		exit;
	}			
			
?>