<?php
session_start();
require_once 'class/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="language" content="FR" />	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="heh,campus,technique,sandwicherie">
		<meta name="geo.placename" content="Mons, Hainaut">
		<meta name="geo.region" content="BE-WHT">
		<meta name="robots" content="index, nofollow" >
		<meta name="description" content="sandwicherie de l'isims,heh campus technique">
		<link rel="stylesheet" href="coin-slider/coin-slider-styles.css" type="text/css" />
		<link rel="stylesheet" href="style.css" />
		<link rel="icon" type="image/png" href="img/favicon.ico" />
		<script type="text/javascript" src="jquery/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="coin-slider/coin-slider.js"></script>
		<script type="text/javascript" src="js/monJS.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="style_ie.css" />
        <![endif]-->
	<!--	<link rel="icon" type="image/png" href="img/decor/favicon.ico" /> -->
	<?php
		$check = $detect->isTablet();
		$check2 = $detect->isMobile();
		//s'il s'agit d'un mobile on applique le style mobile
		if($check2 || $check)
		echo '<link rel="stylesheet" href="style_mobile.css" />';
	?>
		<title>Cafet' Isa</title>
	</head>
	<body>
		<?php
				include ('include/header.php');
				include ('include/bar_de_menu.php');
		?>	
		<div id="conteneur_principal">
			<!-----zone central contenant les élément important---------------------->
			<div id="conteneur_zone_affichage_panier">
				<div id="zone_affichage">
			<?php
						if(isset($_SESSION['erreur_edit_panier']))
						{
							echo 	"<div class='cadre_alerte'>
										<img src='img/attention.png' alt='img attention' id='img_attention'/>  <span id='texte_alerte'>"
										. $_SESSION['erreur_edit_panier'] ."</span></div>";
										$_SESSION['erreur_edit_panier'] = NULL;
						}
												if(isset($_SESSION['info_panier']))
						{
							echo 	"<div class='cadre_alerte'>
										<img src='img/info.png' alt='img info' id='img_info'/>  <span id='texte_alerte'>"
										. $_SESSION['info_panier'] ."</span></div>";
										$_SESSION['info_panier'] = NULL;
						}
				?>
				<h1>Votre panier</h1>
				<?php
				//récupérer le jour actuel
				setlocale(LC_TIME, 'french');
				date_default_timezone_set('Europe/Paris');
				$date = date("Y-m-d");
				//echo strftime('%A %d %B', strtotime($date));
				$jour_actuel = strftime('%A', strtotime($date));
				$heure = date("H:i:s");
				//variables pour récupérer les commandes réalisé
				$heure_du_jour_ouverture = date("Y-m-d 7:00:00");		// récupération dans la base( période où l'on prend compte de la commande réalisé dans la journée)
				$heure_du_jour_fermeture = date("Y-m-d 13:30:00");		// idem
				//variables pour l'accés au panier
				$heure_ouverture = date("07:00:00");				//ouverture du site
				$heure_fermeture_panier = date("11:00:00");
				$heure_fermeture = date("13:30:00");

				if($jour_actuel != "samedi" && $jour_actuel != "dimanche") // jours où l'on ne peut pas réalisé de commande
				{
					//---------------------------------------------
					// On se connecte à  MySQL
					include_once 'include/mysql.inc.php';
					//---------------------------------------------------------------------------------------------------------------------------------------------------------
					$commande_realise = 0;
					// heure d'ouverture et de fermeture du site (on ne peut réaliser une commande que entre ces 2 délais)
					if($heure >$heure_ouverture && $heure < $heure_fermeture)	// lieu ou l'on peut effectuer une commande
					{
						//on vérifie si le client est déjà connecté pour voir s'il a déja effectuer une commande
						if(isset($_SESSION['id']))
						{
							$req = $bdd->query("SELECT id_client, SUM(quantite) as quantite_total, quantite,date_cree,confirmation	FROM commandes WHERE id_client = '".$_SESSION['id']."' && date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."' && confirmation = '1'") or die(print_r($bdd->errorInfo()));
							while($donnee=$req->fetch())
							{
								$commande_realise = $donnee['quantite_total']; // on recupère les articles 
							}
							$req->closeCursor();
						}
						//---------------------------------------------------------------------------------------------------------------------------------------------------------
						if($commande_realise == 0)	//on vérifie si le client n'a pas encore effectuer de commande validé
						{
							if(isset($_SESSION['panier']) && count($_SESSION['panier']['id_produit']) > 0 && ($heure<$heure_fermeture_panier) ) // on vérifie si l'on dispose déja d'un panier
							{
							$nombre_Produit = count($_SESSION['panier']['id_produit']);
							$total_a_payer=0;
							//partie permettant de créer le tableau contenant le panier de l'utilisateur
							//---------------------------------------------------------tableau---------------------------------------------------
							echo '<div class=tableau_panier>
											<div class="titre_tableau_panier">
												<div class="titre_produit_tableau">Nom du produit</div>
												<div class="titre_PU_tableau">Prix unitaire</div>
												<div class="titre_quantite_tableau">Quantité</div>
												<div class="titre_total_tableau" >Total</div>
												<div class="titre_actions_tableau">Actions</div>
											</div>';
								
								if(isset($nombre_Produit))
								{
									
									for($i=0;$i<$nombre_Produit;$i++)
									{
										$id = $_SESSION['panier']['id_produit'][$i];
										$quantite = $_SESSION["panier"]["quantite"][$i];
										$donnee = $bdd->prepare('SELECT * FROM produits WHERE id=?');
										$donnee->execute(array($id));
										$produit = $donnee->fetch();
										$total = $_SESSION["panier"]["quantite"][$i] * $produit["prix"]; 
										echo "<div class='champs_produit ";
										if($i%2 == 0)
											echo "font_clair'>";
										else
											echo "font_foncer'>";
											
											echo "<form method='post' action='edit_Panier.php'>";
											echo '<div class="col_nom_produit">' .$produit["nom"] .'</div>';
											echo '<div class="col_prix_produit">'.$produit["prix"].'€</div>';
											echo '<div class="col_quantite_produit">';
											
											
									if($produit['prix'] != 0)	//si le produits cout 0 € on  demande au client si il en veut ou pas
									{
										echo '<input  type="number" class="entree_formulaire_panier" name="quantite" value="' . $quantite . '"/>';
									}
									else
									{
										echo "<select  name='quantite' class='entree_formulaire_panier_gratuit' disabled='disabled'><option value='0'>Non</option><option value='1' selected='selected'>Oui</option></select>";
									}
											
											echo'</div>';
											echo '<div class="col_total_produit">'.$total.'€</div>';
											echo '<div class="col_action_produit"><input type="hidden" name="id_produit" value="' . $id .'"/><input type=image Value=submit class="b_editer" title="Modifier" src="img/editer.png" onmouseOver="mouse_over_img(this);" onmouseOut="mouse_out_img(this);"/><a href="suppression_Panier.php?id_produit='.$id.'" title="Supprimer"><img src="img/corbeille.png" class="b_supprimer" BORDER="0" alt="img edit" onmouseOver="mouse_over_img(this);" onmouseOut="mouse_out_img(this);"/></a></div></div>';
											echo '</form>';
										$total_a_payer += $total;
										$donnee->closeCursor();
									}
								}
								echo '<div class="cadre_total_panier">';
								echo '<div class="champs_vide">Total à payer : </div>';
								echo	'<div class="col_somme_total">'.$total_a_payer.'€</div>';
								echo '</div>';
								echo '</div>';
								//------------------------------------------------------FIN DU TABLEAU
								//partie permettant d'ajouter des remarques/et formulaire de validation de la commande
								if(isset($_SESSION['nom']) && isset($_SESSION['prenom']))
								{	
									echo '<span id="commentaire_panier">Commentaire:</span>';	
									echo '<form method="post" action="verification/validation_Commande.php">';
									echo '<input type="text" name="exigences" maxlength="150" id="exigence_commande" placeholder="facultatif (maximum 150 caractères)"/><br/>';
									echo '<input type="hidden" name="total_a_payer" value="'.$total_a_payer.'"/>';
									echo '<input type=submit value="Commander" id="b_commander"/>';
									echo '</form>';
								}
								else
								{
									echo '<p>Vous devez vous connectez avec votre identifiant et votre mot de passe personnel pour pouvoir finaliser la commande.</p>';
								}
							}
							else
							{
								if($heure <$heure_fermeture_panier)
									echo "<p>Vous n'avez pas encore effectué d'achat, veuillez vous rendre sur la page \"commander\" si vous désirez effectuer un achat.</p>";
								else
									echo "<p>La plateforme n'est pas activée, celle-ci n'est active qu'entre ".$heure_ouverture." et ".$heure_fermeture_panier." du lundi au vendredi.</p>";
							}
						}
						else
						{	//s'il n'est pas encore 13h
							if($heure<$heure_fermeture_panier)
							{
								//on supprime le panier du client pour l'empécher de continuer à effectuer des achats
								$temp['panier'] = array();
								$temp['panier']['id_produit'] = array();
								$temp['panier']['quantite'] = array();
								$_SESSION['panier'] = $temp['panier'];
								$liste_req = $bdd->query("SELECT c.id_client,c.confirmation,c.id_produit,c.quantite, p.id,p.nom,p.prix
																		FROM commandes c
																		INNER JOIN produits p
																		ON p.id = c.id_produit
																		WHERE id_client = '".$_SESSION['id']."'&& confirmation=1 && date_cree > '".$heure_du_jour_ouverture."' && date_cree < '".$heure_du_jour_fermeture."'");
								//--------------partie permettant d'annuler la commande
								echo "<p>Votre commande a bien été effectuée, vous pourrez la récupérer au-près de la sandwicherie de votre établissement entre 12h30 et 13h30.</p>";
								echo 'Votre commande:<br/>';
								//-----------------------------------------tableau 2
								echo '<table BORDER=1 cellspacing="0" style="width:98%;">';
								echo '<tr style="background-color:rgb(100,100,100);"><th>Nom du produit</th><th align=center>Prix unitaire</th><th align=center>Quantité</th><th align=center>Total</th></tr>';
								$total_a_payer = 0;
								$total = 0;
								$i=0;
								while($liste=$liste_req->fetch())
								{
								$total = $liste['quantite']*$liste['prix'];
								echo '<tr';			
								if($i%2 == 0)
									echo " class= 'font_clair' ";
								else
									echo " class='font_foncer' ";
								$i++;
								echo '><td>'.$liste['nom'].'</td><td align=center>'.$liste['prix'].'</td><td align=center>'.$liste['quantite'].'</td><td align=center>'.$total.'</td></tr>';
								$total_a_payer += $total;
								$total=0;
								}
								echo '<tr><td colspan="3" style="background-color:rgb(200,200,200);" align=right>Somme totale : </td><td style="background-color: rgb(220,220,220);" align=center>'.$total_a_payer.' €</td></tr>';
								echo '</table>';
								//-----------------------------fin tableau 2
								echo "<p>Vous pouvez annuler votre commande jusqu'à 11h. Après cette heure, il ne vous sera plus possible de l'annuler.</p>";
								echo "<form method='post' action='annulation_commande.php'>
												<input type='hidden' name='identifiant' value='".$_SESSION['id']."'/>
												<input type='submit' id='b_annuler_panier'  value='Annuler la commande'/>
										   </FORM>";
						}
							else
							{
								echo "<p>Votre commande a été reçue par le personnel de la sandwicherie, vous ne pouvez plus annuler votre commande.</p>"; 
								echo "<p>Vous pourrez la récuperer au-près de la sandwicherie de votre établissement entre 12h30 et 13h30.</p>";
							}
						}
					}
					else
					{
						echo "<p>La plateforme n'est pas activée, celle-ci n'est active qu'entre ".$heure_ouverture." et ".$heure_fermeture_panier." du lundi au vendredi.</p>";
					}
				}
				else
				{
					echo "<p>Nous sommes ".$jour_actuel.", la plateforme n'est pas activée, vous ne pouvez pas réaliser de commande aujourd'hui.</p>";
				}
				?>
				
				</div>
				<!--partie remarque en bas de page-->
				<div id="conteneur_remarque_panier">
					<h3 id="titre_remarque">Remarques:</h3>
					<p> Lors de la récupération de votre commande à la sandwicherie de votre établissement, vous devez présenter votre carte étudiant que vous avez reçu lors de votre inscription.</p>
					<p><img src='img/attention2.png' style="float:left;" alt='img attention' id='img_attention_remarque'/>Assurerez-vous d'avoir des fonts suffisants sur votre carte étudiant, toutes achats avec des fonts insufissants vous sera facturé par courrier.</p>
				</div>
			</div>
			<!-------zone d'information/annonce---------------------------------->	
			<?php
				include('include/news.php');
			?>
		</div>
		<?php
			include ('include/footer.php');		
		?>
		<script type="text/javascript" src="js/monJQ.js"></script>	
	</body>
</html>